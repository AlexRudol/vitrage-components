import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitSidePanelComponent } from './vit-side-panel';
import { VitMenuModule } from '@library/vit-menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { VitButtonModule } from '@library/vit-button';
import * as i0 from "@angular/core";
export class VitSidePanelModule {
}
VitSidePanelModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitSidePanelModule });
VitSidePanelModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitSidePanelModule_Factory(t) { return new (t || VitSidePanelModule)(); }, imports: [[CommonModule, VitMenuModule, MatButtonModule, MatIconModule, VitButtonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitSidePanelModule, { declarations: [VitSidePanelComponent], imports: [CommonModule, VitMenuModule, MatButtonModule, MatIconModule, VitButtonModule], exports: [VitSidePanelComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitSidePanelModule, [{
        type: NgModule,
        args: [{
                declarations: [VitSidePanelComponent],
                imports: [CommonModule, VitMenuModule, MatButtonModule, MatIconModule, VitButtonModule],
                exports: [VitSidePanelComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LXNpZGUtcGFuZWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vY29tcG9uZW50cy92aXQtc2lkZS1wYW5lbC9zcmMvbGliL3ZpdC1zaWRlLXBhbmVsLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7O0FBT3RELE1BQU0sT0FBTyxrQkFBa0I7O3NEQUFsQixrQkFBa0I7bUhBQWxCLGtCQUFrQixrQkFIbEIsQ0FBQyxZQUFZLEVBQUUsYUFBYSxFQUFFLGVBQWUsRUFBRSxhQUFhLEVBQUUsZUFBZSxDQUFDO3dGQUc5RSxrQkFBa0IsbUJBSlgscUJBQXFCLGFBQzNCLFlBQVksRUFBRSxhQUFhLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxlQUFlLGFBQzNFLHFCQUFxQjtrREFFdkIsa0JBQWtCO2NBTDlCLFFBQVE7ZUFBQztnQkFDTixZQUFZLEVBQUUsQ0FBRSxxQkFBcUIsQ0FBRTtnQkFDdkMsT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLGFBQWEsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLGVBQWUsQ0FBQztnQkFDdkYsT0FBTyxFQUFFLENBQUUscUJBQXFCLENBQUU7YUFDckMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IFZpdFNpZGVQYW5lbENvbXBvbmVudCB9IGZyb20gJy4vdml0LXNpZGUtcGFuZWwnO1xuaW1wb3J0IHsgVml0TWVudU1vZHVsZSB9IGZyb20gJ0BsaWJyYXJ5L3ZpdC1tZW51JztcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbic7XG5pbXBvcnQgeyBNYXRJY29uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbic7XG5pbXBvcnQgeyBWaXRCdXR0b25Nb2R1bGUgfSBmcm9tICdAbGlicmFyeS92aXQtYnV0dG9uJztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFsgVml0U2lkZVBhbmVsQ29tcG9uZW50IF0sXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSwgVml0TWVudU1vZHVsZSwgTWF0QnV0dG9uTW9kdWxlLCBNYXRJY29uTW9kdWxlLCBWaXRCdXR0b25Nb2R1bGVdLFxuICAgIGV4cG9ydHM6IFsgVml0U2lkZVBhbmVsQ29tcG9uZW50IF1cbn0pXG5leHBvcnQgY2xhc3MgVml0U2lkZVBhbmVsTW9kdWxlIHsgfVxuIl19