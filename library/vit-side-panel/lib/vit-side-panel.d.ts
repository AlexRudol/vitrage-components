import { EventEmitter, OnInit } from '@angular/core';
import { MenuItemModel } from '@library/vit-menu/lib/menu-item.model';
import * as i0 from "@angular/core";
export declare enum LogoMode {
    full = "full",
    compact = "compact"
}
export declare class VitSidePanelComponent implements OnInit {
    compactMode?: boolean;
    nightMode?: boolean;
    toggle?: boolean;
    logo?: string;
    logoCompact?: string;
    message?: string;
    navigation?: MenuItemModel[];
    alwaysShowIcons?: boolean;
    expandMultiple?: boolean;
    rightAligned?: boolean;
    backgroundHTML?: string;
    selectMenuItemEvent: EventEmitter<Array<string>>;
    logoClickEvent: EventEmitter<LogoMode>;
    toggleClickEvent: EventEmitter<boolean>;
    logoMode: typeof LogoMode;
    animatedPanel: boolean;
    get classes(): string;
    onResize(): void;
    constructor();
    ngOnInit(): void;
    setPanelMode(): void;
    togglePanelMode(): void;
    clickOnToggle(): void;
    clickOnLogo(logoMode: LogoMode): void;
    selectMenuItem(selectedItem: string[]): void;
    static ɵfac: i0.ɵɵFactoryDef<VitSidePanelComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<VitSidePanelComponent, "vit-side-panel", never, { "compactMode": "compactMode"; "nightMode": "nightMode"; "toggle": "toggle"; "logo": "logo"; "logoCompact": "logoCompact"; "message": "message"; "navigation": "navigation"; "alwaysShowIcons": "alwaysShowIcons"; "expandMultiple": "expandMultiple"; "rightAligned": "rightAligned"; "backgroundHTML": "backgroundHTML"; }, { "selectMenuItemEvent": "selectMenuItemEvent"; "logoClickEvent": "logoClickEvent"; "toggleClickEvent": "toggleClickEvent"; }, never, never>;
}
