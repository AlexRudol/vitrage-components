import * as i0 from "@angular/core";
import * as i1 from "./vit-side-panel";
import * as i2 from "@angular/common";
import * as i3 from "@library/vit-menu";
import * as i4 from "@angular/material/button";
import * as i5 from "@angular/material/icon";
import * as i6 from "@library/vit-button";
export declare class VitSidePanelModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<VitSidePanelModule, [typeof i1.VitSidePanelComponent], [typeof i2.CommonModule, typeof i3.VitMenuModule, typeof i4.MatButtonModule, typeof i5.MatIconModule, typeof i6.VitButtonModule], [typeof i1.VitSidePanelComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<VitSidePanelModule>;
}
