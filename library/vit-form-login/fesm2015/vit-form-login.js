import { ɵɵdefineComponent, ɵɵelementStart, ɵɵelement, ɵɵelementEnd, ɵɵlistener, ɵɵproperty, ɵɵadvance, ɵsetClassMetadata, Component, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { FormGroup, FormControl, Validators, ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, ReactiveFormsModule } from '@angular/forms';
import { VitInputComponent, VitInputModule } from '@library/vit-input';
import { VitButtonComponent, VitButtonModule } from '@library/vit-button';
import { CommonModule } from '@angular/common';

class VitLoginFormComponent {
    constructor() {
        /*
         * Specifying the form fields
         */
        this.formLogin = new FormGroup({
            email: new FormControl('', {
                validators: [Validators.required, Validators.email], updateOn: 'blur'
            }),
            password: new FormControl('', {
                validators: [Validators.required], updateOn: 'change'
            })
        });
    }
    /*
     * Reset the validation if the field is empty again
     */
    ngOnInit() {
        // Reset validation if the input is empty again
        this.formLogin.valueChanges
            .subscribe(() => {
            this.resetForm(this.formLogin);
        });
    }
    /*
     * Collect the form values and send it outside
     */
    getValues() {
        console.log(this.formLogin.value);
    }
    // TODO: Refactor (function as a helper?)
    resetForm(form) {
        Object.keys(form.controls).forEach(key => {
            if (form.get(key).value === '') {
                form.get(key).setErrors(null);
            }
        });
    }
}
VitLoginFormComponent.ɵfac = function VitLoginFormComponent_Factory(t) { return new (t || VitLoginFormComponent)(); };
VitLoginFormComponent.ɵcmp = ɵɵdefineComponent({ type: VitLoginFormComponent, selectors: [["vit-form-login"]], decls: 9, vars: 6, consts: [["id", "formLogin", 1, "columns", "is-centered", 3, "formGroup"], [1, "column", "is-3-desktop", "is-4-tablet", "is-7-mobile"], ["controlName", "email", "type", "email", "label", "Email", 3, "form"], ["controlName", "password", "type", "password", "label", "Password", "hint", "<a href='/' title='Restore the password' >Forgot password?</a>", 3, "form"], [1, "column", "is-1", "is-1-mobile", "form-actions"], ["id", "buttonLogin", "type", "primary", "tooltip", "Log in", "icon", "keyboard_arrow_right", 3, "disabled", "clickEvent"], ["id", "registrationLinkBlock", 1, "columns", "is-centered"], ["type", "default", "label", "Register", 3, "compact", "wide"]], template: function VitLoginFormComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "form", 0);
        ɵɵelementStart(1, "div", 1);
        ɵɵelement(2, "vit-input", 2);
        ɵɵelement(3, "vit-input", 3);
        ɵɵelementEnd();
        ɵɵelementStart(4, "div", 4);
        ɵɵelementStart(5, "vit-button", 5);
        ɵɵlistener("clickEvent", function VitLoginFormComponent_Template_vit_button_clickEvent_5_listener() { return ctx.getValues(); });
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementEnd();
        ɵɵelementStart(6, "div", 6);
        ɵɵelementStart(7, "div", 1);
        ɵɵelement(8, "vit-button", 7);
        ɵɵelementEnd();
        ɵɵelementEnd();
    } if (rf & 2) {
        ɵɵproperty("formGroup", ctx.formLogin);
        ɵɵadvance(2);
        ɵɵproperty("form", ctx.formLogin);
        ɵɵadvance(1);
        ɵɵproperty("form", ctx.formLogin);
        ɵɵadvance(2);
        ɵɵproperty("disabled", !ctx.formLogin.valid || !ctx.formLogin.controls.email.value || !ctx.formLogin.controls.password.value);
        ɵɵadvance(3);
        ɵɵproperty("compact", true)("wide", true);
    } }, directives: [ɵangular_packages_forms_forms_y, NgControlStatusGroup, FormGroupDirective, VitInputComponent, VitButtonComponent], styles: [".form-actions[_ngcontent-%COMP%]{position:relative}#buttonLogin[_ngcontent-%COMP%]{position:absolute;margin-left:-15px;top:calc(50% - 13px);transform:translateY(-50%)}#registrationLinkBlock[_ngcontent-%COMP%]{margin-top:30px;text-align:center}"] });
/*@__PURE__*/ (function () { ɵsetClassMetadata(VitLoginFormComponent, [{
        type: Component,
        args: [{
                selector: 'vit-form-login',
                templateUrl: './vit-form-login.html',
                styleUrls: ['vit-form-login.scss']
            }]
    }], function () { return []; }, null); })();

class VitLoginFormModule {
}
VitLoginFormModule.ɵmod = ɵɵdefineNgModule({ type: VitLoginFormModule });
VitLoginFormModule.ɵinj = ɵɵdefineInjector({ factory: function VitLoginFormModule_Factory(t) { return new (t || VitLoginFormModule)(); }, imports: [[CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(VitLoginFormModule, { declarations: [VitLoginFormComponent], imports: [CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule], exports: [VitLoginFormComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(VitLoginFormModule, [{
        type: NgModule,
        args: [{
                declarations: [VitLoginFormComponent],
                imports: [CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule],
                exports: [VitLoginFormComponent]
            }]
    }], null, null); })();

/**
 * Generated bundle index. Do not edit.
 */

export { VitLoginFormComponent, VitLoginFormModule };
//# sourceMappingURL=vit-form-login.js.map
