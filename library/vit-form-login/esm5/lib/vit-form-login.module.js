import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitLoginFormComponent } from './vit-form-login';
import { VitInputModule } from '@library/vit-input';
import { ReactiveFormsModule } from '@angular/forms';
import { VitButtonModule } from '@library/vit-button';
import * as i0 from "@angular/core";
var VitLoginFormModule = /** @class */ (function () {
    function VitLoginFormModule() {
    }
    VitLoginFormModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitLoginFormModule });
    VitLoginFormModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitLoginFormModule_Factory(t) { return new (t || VitLoginFormModule)(); }, imports: [[CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule]] });
    return VitLoginFormModule;
}());
export { VitLoginFormModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitLoginFormModule, { declarations: [VitLoginFormComponent], imports: [CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule], exports: [VitLoginFormComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitLoginFormModule, [{
        type: NgModule,
        args: [{
                declarations: [VitLoginFormComponent],
                imports: [CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule],
                exports: [VitLoginFormComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWZvcm0tbG9naW4ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdml0LWZvcm0tbG9naW4vIiwic291cmNlcyI6WyJsaWIvdml0LWZvcm0tbG9naW4ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7O0FBRXREO0lBQUE7S0FLbUM7MERBQXRCLGtCQUFrQjt1SEFBbEIsa0JBQWtCLGtCQUhsQixDQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsY0FBYyxFQUFFLG1CQUFtQixDQUFFOzZCQVRuRjtDQVltQyxBQUxuQyxJQUttQztTQUF0QixrQkFBa0I7d0ZBQWxCLGtCQUFrQixtQkFKWCxxQkFBcUIsYUFDMUIsWUFBWSxFQUFFLGVBQWUsRUFBRSxjQUFjLEVBQUUsbUJBQW1CLGFBQ2xFLHFCQUFxQjtrREFFdkIsa0JBQWtCO2NBTDlCLFFBQVE7ZUFBQztnQkFDTixZQUFZLEVBQUUsQ0FBRSxxQkFBcUIsQ0FBRTtnQkFDdkMsT0FBTyxFQUFFLENBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxjQUFjLEVBQUUsbUJBQW1CLENBQUU7Z0JBQy9FLE9BQU8sRUFBRSxDQUFFLHFCQUFxQixDQUFFO2FBQ3JDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBWaXRMb2dpbkZvcm1Db21wb25lbnQgfSBmcm9tICcuL3ZpdC1mb3JtLWxvZ2luJztcbmltcG9ydCB7IFZpdElucHV0TW9kdWxlIH0gZnJvbSAnQGxpYnJhcnkvdml0LWlucHV0JztcbmltcG9ydCB7IFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBWaXRCdXR0b25Nb2R1bGUgfSBmcm9tICdAbGlicmFyeS92aXQtYnV0dG9uJztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFsgVml0TG9naW5Gb3JtQ29tcG9uZW50IF0sXG4gICAgaW1wb3J0czogWyBDb21tb25Nb2R1bGUsIFZpdEJ1dHRvbk1vZHVsZSwgVml0SW5wdXRNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgXSxcbiAgICBleHBvcnRzOiBbIFZpdExvZ2luRm9ybUNvbXBvbmVudCBdXG59KVxuZXhwb3J0IGNsYXNzIFZpdExvZ2luRm9ybU1vZHVsZSB7IH1cbiJdfQ==