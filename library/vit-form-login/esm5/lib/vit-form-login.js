import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/forms";
import * as i2 from "@library/vit-input";
import * as i3 from "@library/vit-button";
var VitLoginFormComponent = /** @class */ (function () {
    function VitLoginFormComponent() {
        /*
         * Specifying the form fields
         */
        this.formLogin = new FormGroup({
            email: new FormControl('', {
                validators: [Validators.required, Validators.email], updateOn: 'blur'
            }),
            password: new FormControl('', {
                validators: [Validators.required], updateOn: 'change'
            })
        });
    }
    /*
     * Reset the validation if the field is empty again
     */
    VitLoginFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Reset validation if the input is empty again
        this.formLogin.valueChanges
            .subscribe(function () {
            _this.resetForm(_this.formLogin);
        });
    };
    /*
     * Collect the form values and send it outside
     */
    VitLoginFormComponent.prototype.getValues = function () {
        console.log(this.formLogin.value);
    };
    // TODO: Refactor (function as a helper?)
    VitLoginFormComponent.prototype.resetForm = function (form) {
        Object.keys(form.controls).forEach(function (key) {
            if (form.get(key).value === '') {
                form.get(key).setErrors(null);
            }
        });
    };
    VitLoginFormComponent.ɵfac = function VitLoginFormComponent_Factory(t) { return new (t || VitLoginFormComponent)(); };
    VitLoginFormComponent.ɵcmp = i0.ɵɵdefineComponent({ type: VitLoginFormComponent, selectors: [["vit-form-login"]], decls: 9, vars: 6, consts: [["id", "formLogin", 1, "columns", "is-centered", 3, "formGroup"], [1, "column", "is-3-desktop", "is-4-tablet", "is-7-mobile"], ["controlName", "email", "type", "email", "label", "Email", 3, "form"], ["controlName", "password", "type", "password", "label", "Password", "hint", "<a href='/' title='Restore the password' >Forgot password?</a>", 3, "form"], [1, "column", "is-1", "is-1-mobile", "form-actions"], ["id", "buttonLogin", "type", "primary", "tooltip", "Log in", "icon", "keyboard_arrow_right", 3, "disabled", "clickEvent"], ["id", "registrationLinkBlock", 1, "columns", "is-centered"], ["type", "default", "label", "Register", 3, "compact", "wide"]], template: function VitLoginFormComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "form", 0);
            i0.ɵɵelementStart(1, "div", 1);
            i0.ɵɵelement(2, "vit-input", 2);
            i0.ɵɵelement(3, "vit-input", 3);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(4, "div", 4);
            i0.ɵɵelementStart(5, "vit-button", 5);
            i0.ɵɵlistener("clickEvent", function VitLoginFormComponent_Template_vit_button_clickEvent_5_listener() { return ctx.getValues(); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(6, "div", 6);
            i0.ɵɵelementStart(7, "div", 1);
            i0.ɵɵelement(8, "vit-button", 7);
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵproperty("formGroup", ctx.formLogin);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("form", ctx.formLogin);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("form", ctx.formLogin);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("disabled", !ctx.formLogin.valid || !ctx.formLogin.controls.email.value || !ctx.formLogin.controls.password.value);
            i0.ɵɵadvance(3);
            i0.ɵɵproperty("compact", true)("wide", true);
        } }, directives: [i1.ɵangular_packages_forms_forms_y, i1.NgControlStatusGroup, i1.FormGroupDirective, i2.VitInputComponent, i3.VitButtonComponent], styles: [".form-actions[_ngcontent-%COMP%]{position:relative}#buttonLogin[_ngcontent-%COMP%]{position:absolute;margin-left:-15px;top:calc(50% - 13px);transform:translateY(-50%)}#registrationLinkBlock[_ngcontent-%COMP%]{margin-top:30px;text-align:center}"] });
    return VitLoginFormComponent;
}());
export { VitLoginFormComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitLoginFormComponent, [{
        type: Component,
        args: [{
                selector: 'vit-form-login',
                templateUrl: './vit-form-login.html',
                styleUrls: ['vit-form-login.scss']
            }]
    }], function () { return []; }, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWZvcm0tbG9naW4uanMiLCJzb3VyY2VSb290Ijoibmc6Ly92aXQtZm9ybS1sb2dpbi8iLCJzb3VyY2VzIjpbImxpYi92aXQtZm9ybS1sb2dpbi50cyIsImxpYi92aXQtZm9ybS1sb2dpbi5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7O0FBRXBFO0lBU0k7UUFDSTs7V0FFRztRQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxTQUFTLENBQUM7WUFDM0IsS0FBSyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRTtnQkFDdkIsVUFBVSxFQUFFLENBQUUsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsS0FBSyxDQUFFLEVBQUUsUUFBUSxFQUFFLE1BQU07YUFDMUUsQ0FBQztZQUNGLFFBQVEsRUFBRSxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUU7Z0JBQzFCLFVBQVUsRUFBRSxDQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUUsRUFBRSxRQUFRLEVBQUUsUUFBUTthQUMxRCxDQUFDO1NBQ0wsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztPQUVHO0lBQ0gsd0NBQVEsR0FBUjtRQUFBLGlCQU1DO1FBTEcsK0NBQStDO1FBQy9DLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWTthQUN0QixTQUFTLENBQUM7WUFDUCxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNuQyxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRDs7T0FFRztJQUNILHlDQUFTLEdBQVQ7UUFDSSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVELHlDQUF5QztJQUN6Qyx5Q0FBUyxHQUFULFVBQVUsSUFBZTtRQUNyQixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBQSxHQUFHO1lBQ2xDLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUM1QixJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNqQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs4RkEzQ1EscUJBQXFCOzhEQUFyQixxQkFBcUI7WUNQbEMsK0JBRUk7WUFBQSw4QkFFSTtZQUNJLCtCQUEwRjtZQUcxRiwrQkFDdUY7WUFFL0YsaUJBQU07WUFHTiw4QkFDSTtZQUFBLHFDQUVnRDtZQUF6QyxnSEFBYyxlQUFXLElBQUM7WUFBRSxpQkFBYTtZQUNwRCxpQkFBTTtZQUVWLGlCQUFPO1lBR1AsOEJBQ0k7WUFBQSw4QkFDSTtZQUFBLGdDQUF5RjtZQUM3RixpQkFBTTtZQUNWLGlCQUFNOztZQTNCMkMseUNBQXVCO1lBS2pELGVBQWtCO1lBQWxCLG9DQUFrQjtZQUdsQixlQUFrQjtZQUFsQixvQ0FBa0I7WUFRMUIsZUFBc0c7WUFBdEcsZ0lBQXNHO1lBU2xGLGVBQWdCO1lBQWhCLDhCQUFnQixjQUFBOztnQ0QxQm5EO0NBcURDLEFBbERELElBa0RDO1NBN0NZLHFCQUFxQjtrREFBckIscUJBQXFCO2NBTGpDLFNBQVM7ZUFBQztnQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixXQUFXLEVBQUUsdUJBQXVCO2dCQUNwQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQzthQUNyQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCwgRm9ybUdyb3VwLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3ZpdC1mb3JtLWxvZ2luJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdml0LWZvcm0tbG9naW4uaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJ3ZpdC1mb3JtLWxvZ2luLnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBWaXRMb2dpbkZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgZm9ybUxvZ2luOiBGb3JtR3JvdXA7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgLypcbiAgICAgICAgICogU3BlY2lmeWluZyB0aGUgZm9ybSBmaWVsZHNcbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuZm9ybUxvZ2luID0gbmV3IEZvcm1Hcm91cCh7XG4gICAgICAgICAgICBlbWFpbDogbmV3IEZvcm1Db250cm9sKCcnLCB7XG4gICAgICAgICAgICAgICAgdmFsaWRhdG9yczogWyBWYWxpZGF0b3JzLnJlcXVpcmVkLCBWYWxpZGF0b3JzLmVtYWlsIF0sIHVwZGF0ZU9uOiAnYmx1cidcbiAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgcGFzc3dvcmQ6IG5ldyBGb3JtQ29udHJvbCgnJywge1xuICAgICAgICAgICAgICAgIHZhbGlkYXRvcnM6IFsgVmFsaWRhdG9ycy5yZXF1aXJlZCBdLCB1cGRhdGVPbjogJ2NoYW5nZSdcbiAgICAgICAgICAgIH0pXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIC8qXG4gICAgICogUmVzZXQgdGhlIHZhbGlkYXRpb24gaWYgdGhlIGZpZWxkIGlzIGVtcHR5IGFnYWluXG4gICAgICovXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIC8vIFJlc2V0IHZhbGlkYXRpb24gaWYgdGhlIGlucHV0IGlzIGVtcHR5IGFnYWluXG4gICAgICAgIHRoaXMuZm9ybUxvZ2luLnZhbHVlQ2hhbmdlc1xuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNldEZvcm0odGhpcy5mb3JtTG9naW4pO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLypcbiAgICAgKiBDb2xsZWN0IHRoZSBmb3JtIHZhbHVlcyBhbmQgc2VuZCBpdCBvdXRzaWRlXG4gICAgICovXG4gICAgZ2V0VmFsdWVzKCk6IHZvaWQge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmZvcm1Mb2dpbi52YWx1ZSk7XG4gICAgfVxuXG4gICAgLy8gVE9ETzogUmVmYWN0b3IgKGZ1bmN0aW9uIGFzIGEgaGVscGVyPylcbiAgICByZXNldEZvcm0oZm9ybTogRm9ybUdyb3VwKSB7XG4gICAgICAgIE9iamVjdC5rZXlzKGZvcm0uY29udHJvbHMpLmZvckVhY2goa2V5ID0+IHtcbiAgICAgICAgICAgIGlmIChmb3JtLmdldChrZXkpLnZhbHVlID09PSAnJykge1xuICAgICAgICAgICAgICAgIGZvcm0uZ2V0KGtleSkuc2V0RXJyb3JzKG51bGwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbn1cbiIsIjwhLS0gTG9naW4gZm9ybSAtLT5cbjxmb3JtIGlkPVwiZm9ybUxvZ2luXCIgY2xhc3M9XCJjb2x1bW5zIGlzLWNlbnRlcmVkXCIgW2Zvcm1Hcm91cF09XCJmb3JtTG9naW5cIiA+XG5cbiAgICA8ZGl2IGNsYXNzPVwiY29sdW1uIGlzLTMtZGVza3RvcCBpcy00LXRhYmxldCBpcy03LW1vYmlsZVwiID5cblxuICAgICAgICA8IS0tIEVtYWlsIGFzIGEgTG9naW4gLS0+XG4gICAgICAgICAgICA8dml0LWlucHV0IFtmb3JtXT1cImZvcm1Mb2dpblwiIGNvbnRyb2xOYW1lPVwiZW1haWxcIiB0eXBlPVwiZW1haWxcIiBsYWJlbD1cIkVtYWlsXCIgPjwvdml0LWlucHV0PlxuXG4gICAgICAgIDwhLS0gUGFzc3dvcmQgLS0+XG4gICAgICAgICAgICA8dml0LWlucHV0IFtmb3JtXT1cImZvcm1Mb2dpblwiIGNvbnRyb2xOYW1lPVwicGFzc3dvcmRcIiB0eXBlPVwicGFzc3dvcmRcIiBsYWJlbD1cIlBhc3N3b3JkXCJcbiAgICAgICAgICAgICAgICBoaW50PVwiPGEgaHJlZj0nLycgdGl0bGU9J1Jlc3RvcmUgdGhlIHBhc3N3b3JkJyA+Rm9yZ290IHBhc3N3b3JkPzwvYT5cIiA+PC92aXQtaW5wdXQ+XG5cbiAgICA8L2Rpdj5cblxuICAgIDwhLS0gRm9ybSBhY3Rpb25zIC0tPlxuICAgIDxkaXYgY2xhc3M9XCJjb2x1bW4gaXMtMSBpcy0xLW1vYmlsZSBmb3JtLWFjdGlvbnNcIiA+XG4gICAgICAgIDx2aXQtYnV0dG9uIGlkPVwiYnV0dG9uTG9naW5cIiB0eXBlPVwicHJpbWFyeVwiIHRvb2x0aXA9XCJMb2cgaW5cIiBpY29uPVwia2V5Ym9hcmRfYXJyb3dfcmlnaHRcIlxuICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cIiFmb3JtTG9naW4udmFsaWQgfHwgIWZvcm1Mb2dpbi5jb250cm9scy5lbWFpbC52YWx1ZSB8fCAhZm9ybUxvZ2luLmNvbnRyb2xzLnBhc3N3b3JkLnZhbHVlXCJcbiAgICAgICAgICAgICAgIChjbGlja0V2ZW50KT1cImdldFZhbHVlcygpXCIgPjwvdml0LWJ1dHRvbj5cbiAgICA8L2Rpdj5cblxuPC9mb3JtPlxuXG48IS0tIFJlZ2lzdHJhdGlvbiBsaW5rIC0tPlxuPGRpdiBpZD1cInJlZ2lzdHJhdGlvbkxpbmtCbG9ja1wiIGNsYXNzPVwiY29sdW1ucyBpcy1jZW50ZXJlZFwiID5cbiAgICA8ZGl2IGNsYXNzPVwiY29sdW1uIGlzLTMtZGVza3RvcCBpcy00LXRhYmxldCBpcy03LW1vYmlsZVwiID5cbiAgICAgICAgPHZpdC1idXR0b24gdHlwZT1cImRlZmF1bHRcIiBbY29tcGFjdF09XCJ0cnVlXCIgW3dpZGVdPVwidHJ1ZVwiIGxhYmVsPVwiUmVnaXN0ZXJcIiA+PC92aXQtYnV0dG9uPlxuICAgIDwvZGl2PlxuPC9kaXY+XG4iXX0=