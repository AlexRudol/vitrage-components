import { OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as i0 from "@angular/core";
export declare class VitLoginFormComponent implements OnInit {
    formLogin: FormGroup;
    constructor();
    ngOnInit(): void;
    getValues(): void;
    resetForm(form: FormGroup): void;
    static ɵfac: i0.ɵɵFactoryDef<VitLoginFormComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<VitLoginFormComponent, "vit-form-login", never, {}, {}, never, never>;
}
