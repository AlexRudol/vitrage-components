import * as i0 from "@angular/core";
import * as i1 from "./vit-form-login";
import * as i2 from "@angular/common";
import * as i3 from "@library/vit-button";
import * as i4 from "@library/vit-input";
import * as i5 from "@angular/forms";
export declare class VitLoginFormModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<VitLoginFormModule, [typeof i1.VitLoginFormComponent], [typeof i2.CommonModule, typeof i3.VitButtonModule, typeof i4.VitInputModule, typeof i5.ReactiveFormsModule], [typeof i1.VitLoginFormComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<VitLoginFormModule>;
}
