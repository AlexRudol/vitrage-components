import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitLoginFormComponent } from './vit-form-login';
import { VitInputModule } from '@library/vit-input';
import { ReactiveFormsModule } from '@angular/forms';
import { VitButtonModule } from '@library/vit-button';
import * as i0 from "@angular/core";
export class VitLoginFormModule {
}
VitLoginFormModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitLoginFormModule });
VitLoginFormModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitLoginFormModule_Factory(t) { return new (t || VitLoginFormModule)(); }, imports: [[CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitLoginFormModule, { declarations: [VitLoginFormComponent], imports: [CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule], exports: [VitLoginFormComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitLoginFormModule, [{
        type: NgModule,
        args: [{
                declarations: [VitLoginFormComponent],
                imports: [CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule],
                exports: [VitLoginFormComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWZvcm0tbG9naW4ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdml0LWZvcm0tbG9naW4vIiwic291cmNlcyI6WyJsaWIvdml0LWZvcm0tbG9naW4ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7O0FBT3RELE1BQU0sT0FBTyxrQkFBa0I7O3NEQUFsQixrQkFBa0I7bUhBQWxCLGtCQUFrQixrQkFIbEIsQ0FBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsQ0FBRTt3RkFHdEUsa0JBQWtCLG1CQUpYLHFCQUFxQixhQUMxQixZQUFZLEVBQUUsZUFBZSxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsYUFDbEUscUJBQXFCO2tEQUV2QixrQkFBa0I7Y0FMOUIsUUFBUTtlQUFDO2dCQUNOLFlBQVksRUFBRSxDQUFFLHFCQUFxQixDQUFFO2dCQUN2QyxPQUFPLEVBQUUsQ0FBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLGNBQWMsRUFBRSxtQkFBbUIsQ0FBRTtnQkFDL0UsT0FBTyxFQUFFLENBQUUscUJBQXFCLENBQUU7YUFDckMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IFZpdExvZ2luRm9ybUNvbXBvbmVudCB9IGZyb20gJy4vdml0LWZvcm0tbG9naW4nO1xuaW1wb3J0IHsgVml0SW5wdXRNb2R1bGUgfSBmcm9tICdAbGlicmFyeS92aXQtaW5wdXQnO1xuaW1wb3J0IHsgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IFZpdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0BsaWJyYXJ5L3ZpdC1idXR0b24nO1xuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogWyBWaXRMb2dpbkZvcm1Db21wb25lbnQgXSxcbiAgICBpbXBvcnRzOiBbIENvbW1vbk1vZHVsZSwgVml0QnV0dG9uTW9kdWxlLCBWaXRJbnB1dE1vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSBdLFxuICAgIGV4cG9ydHM6IFsgVml0TG9naW5Gb3JtQ29tcG9uZW50IF1cbn0pXG5leHBvcnQgY2xhc3MgVml0TG9naW5Gb3JtTW9kdWxlIHsgfVxuIl19