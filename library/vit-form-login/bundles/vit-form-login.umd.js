(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@library/vit-input'), require('@library/vit-button'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('vit-form-login', ['exports', '@angular/core', '@angular/forms', '@library/vit-input', '@library/vit-button', '@angular/common'], factory) :
    (global = global || self, factory(global['vit-form-login'] = {}, global.ng.core, global.ng.forms, global.vitInput, global.vitButton, global.ng.common));
}(this, (function (exports, core, forms, vitInput, vitButton, common) { 'use strict';

    var VitLoginFormComponent = /** @class */ (function () {
        function VitLoginFormComponent() {
            /*
             * Specifying the form fields
             */
            this.formLogin = new forms.FormGroup({
                email: new forms.FormControl('', {
                    validators: [forms.Validators.required, forms.Validators.email], updateOn: 'blur'
                }),
                password: new forms.FormControl('', {
                    validators: [forms.Validators.required], updateOn: 'change'
                })
            });
        }
        /*
         * Reset the validation if the field is empty again
         */
        VitLoginFormComponent.prototype.ngOnInit = function () {
            var _this = this;
            // Reset validation if the input is empty again
            this.formLogin.valueChanges
                .subscribe(function () {
                _this.resetForm(_this.formLogin);
            });
        };
        /*
         * Collect the form values and send it outside
         */
        VitLoginFormComponent.prototype.getValues = function () {
            console.log(this.formLogin.value);
        };
        // TODO: Refactor (function as a helper?)
        VitLoginFormComponent.prototype.resetForm = function (form) {
            Object.keys(form.controls).forEach(function (key) {
                if (form.get(key).value === '') {
                    form.get(key).setErrors(null);
                }
            });
        };
        VitLoginFormComponent.ɵfac = function VitLoginFormComponent_Factory(t) { return new (t || VitLoginFormComponent)(); };
        VitLoginFormComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: VitLoginFormComponent, selectors: [["vit-form-login"]], decls: 9, vars: 6, consts: [["id", "formLogin", 1, "columns", "is-centered", 3, "formGroup"], [1, "column", "is-3-desktop", "is-4-tablet", "is-7-mobile"], ["controlName", "email", "type", "email", "label", "Email", 3, "form"], ["controlName", "password", "type", "password", "label", "Password", "hint", "<a href='/' title='Restore the password' >Forgot password?</a>", 3, "form"], [1, "column", "is-1", "is-1-mobile", "form-actions"], ["id", "buttonLogin", "type", "primary", "tooltip", "Log in", "icon", "keyboard_arrow_right", 3, "disabled", "clickEvent"], ["id", "registrationLinkBlock", 1, "columns", "is-centered"], ["type", "default", "label", "Register", 3, "compact", "wide"]], template: function VitLoginFormComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "form", 0);
                core["ɵɵelementStart"](1, "div", 1);
                core["ɵɵelement"](2, "vit-input", 2);
                core["ɵɵelement"](3, "vit-input", 3);
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](4, "div", 4);
                core["ɵɵelementStart"](5, "vit-button", 5);
                core["ɵɵlistener"]("clickEvent", function VitLoginFormComponent_Template_vit_button_clickEvent_5_listener() { return ctx.getValues(); });
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
                core["ɵɵelementStart"](6, "div", 6);
                core["ɵɵelementStart"](7, "div", 1);
                core["ɵɵelement"](8, "vit-button", 7);
                core["ɵɵelementEnd"]();
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                core["ɵɵproperty"]("formGroup", ctx.formLogin);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("form", ctx.formLogin);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("form", ctx.formLogin);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("disabled", !ctx.formLogin.valid || !ctx.formLogin.controls.email.value || !ctx.formLogin.controls.password.value);
                core["ɵɵadvance"](3);
                core["ɵɵproperty"]("compact", true)("wide", true);
            } }, directives: [forms["ɵangular_packages_forms_forms_y"], forms.NgControlStatusGroup, forms.FormGroupDirective, vitInput.VitInputComponent, vitButton.VitButtonComponent], styles: [".form-actions[_ngcontent-%COMP%]{position:relative}#buttonLogin[_ngcontent-%COMP%]{position:absolute;margin-left:-15px;top:calc(50% - 13px);transform:translateY(-50%)}#registrationLinkBlock[_ngcontent-%COMP%]{margin-top:30px;text-align:center}"] });
        return VitLoginFormComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](VitLoginFormComponent, [{
            type: core.Component,
            args: [{
                    selector: 'vit-form-login',
                    templateUrl: './vit-form-login.html',
                    styleUrls: ['vit-form-login.scss']
                }]
        }], function () { return []; }, null); })();

    var VitLoginFormModule = /** @class */ (function () {
        function VitLoginFormModule() {
        }
        VitLoginFormModule.ɵmod = core["ɵɵdefineNgModule"]({ type: VitLoginFormModule });
        VitLoginFormModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function VitLoginFormModule_Factory(t) { return new (t || VitLoginFormModule)(); }, imports: [[common.CommonModule, vitButton.VitButtonModule, vitInput.VitInputModule, forms.ReactiveFormsModule]] });
        return VitLoginFormModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](VitLoginFormModule, { declarations: [VitLoginFormComponent], imports: [common.CommonModule, vitButton.VitButtonModule, vitInput.VitInputModule, forms.ReactiveFormsModule], exports: [VitLoginFormComponent] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](VitLoginFormModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [VitLoginFormComponent],
                    imports: [common.CommonModule, vitButton.VitButtonModule, vitInput.VitInputModule, forms.ReactiveFormsModule],
                    exports: [VitLoginFormComponent]
                }]
        }], null, null); })();

    exports.VitLoginFormComponent = VitLoginFormComponent;
    exports.VitLoginFormModule = VitLoginFormModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=vit-form-login.umd.js.map
