import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitMenuComponent } from './vit-menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import * as i0 from "@angular/core";
export class VitMenuModule {
}
VitMenuModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitMenuModule });
VitMenuModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitMenuModule_Factory(t) { return new (t || VitMenuModule)(); }, imports: [[CommonModule, MatButtonModule, MatListModule, MatIconModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitMenuModule, { declarations: [VitMenuComponent], imports: [CommonModule, MatButtonModule, MatListModule, MatIconModule], exports: [VitMenuComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitMenuModule, [{
        type: NgModule,
        args: [{
                declarations: [VitMenuComponent],
                imports: [CommonModule, MatButtonModule, MatListModule, MatIconModule],
                exports: [VitMenuComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LW1lbnUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vY29tcG9uZW50cy92aXQtbWVudS9zcmMvbGliL3ZpdC1tZW51Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDOUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7O0FBT3ZELE1BQU0sT0FBTyxhQUFhOztpREFBYixhQUFhO3lHQUFiLGFBQWEsa0JBSGIsQ0FBRSxZQUFZLEVBQUUsZUFBZSxFQUFFLGFBQWEsRUFBRSxhQUFhLENBQUU7d0ZBRy9ELGFBQWEsbUJBSk4sZ0JBQWdCLGFBQ3JCLFlBQVksRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLGFBQWEsYUFDM0QsZ0JBQWdCO2tEQUVsQixhQUFhO2NBTHpCLFFBQVE7ZUFBQztnQkFDTixZQUFZLEVBQUUsQ0FBRSxnQkFBZ0IsQ0FBRTtnQkFDbEMsT0FBTyxFQUFFLENBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxhQUFhLEVBQUUsYUFBYSxDQUFFO2dCQUN4RSxPQUFPLEVBQUUsQ0FBRSxnQkFBZ0IsQ0FBRTthQUNoQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgVml0TWVudUNvbXBvbmVudCB9IGZyb20gJy4vdml0LW1lbnUnO1xuaW1wb3J0IHsgTWF0QnV0dG9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uJztcbmltcG9ydCB7IE1hdEljb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pY29uJztcbmltcG9ydCB7IE1hdExpc3RNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9saXN0JztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFsgVml0TWVudUNvbXBvbmVudCBdLFxuICAgIGltcG9ydHM6IFsgQ29tbW9uTW9kdWxlLCBNYXRCdXR0b25Nb2R1bGUsIE1hdExpc3RNb2R1bGUsIE1hdEljb25Nb2R1bGUgXSxcbiAgICBleHBvcnRzOiBbIFZpdE1lbnVDb21wb25lbnQgXVxufSlcbmV4cG9ydCBjbGFzcyBWaXRNZW51TW9kdWxlIHsgfVxuIl19