import { EventEmitter } from '@angular/core';
import { MenuItemModel } from './menu-item.model';
import * as i0 from "@angular/core";
export declare class VitMenuComponent {
    compactMode?: boolean;
    nightMode?: boolean;
    navigation?: MenuItemModel[];
    alwaysShowIcons?: boolean;
    expandMultiple?: boolean;
    rightAligned?: boolean;
    selectMenuItemEvent: EventEmitter<Array<string>>;
    get classes(): string;
    constructor();
    selectMenuItem(clickedItemIndex: string): void;
    expandMenuItem(clickedItemIndex: number, event: Event): void;
    static ɵfac: i0.ɵɵFactoryDef<VitMenuComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<VitMenuComponent, "vit-menu", never, { "compactMode": "compactMode"; "nightMode": "nightMode"; "navigation": "navigation"; "alwaysShowIcons": "alwaysShowIcons"; "expandMultiple": "expandMultiple"; "rightAligned": "rightAligned"; }, { "selectMenuItemEvent": "selectMenuItemEvent"; }, never, never>;
}
