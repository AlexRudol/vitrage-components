import * as i0 from "@angular/core";
import * as i1 from "./vit-menu";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
import * as i4 from "@angular/material/list";
import * as i5 from "@angular/material/icon";
export declare class VitMenuModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<VitMenuModule, [typeof i1.VitMenuComponent], [typeof i2.CommonModule, typeof i3.MatButtonModule, typeof i4.MatListModule, typeof i5.MatIconModule], [typeof i1.VitMenuComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<VitMenuModule>;
}
