export interface MenuItemModel {
    separator?: boolean;
    class?: string;
    label: string;
    icon: string;
    link: string;
    target?: string;
    expanded?: boolean;
    selected?: boolean;
    children?: MenuItemModel[];
}
