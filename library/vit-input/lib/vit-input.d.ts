import { FormGroup } from '@angular/forms';
import * as i0 from "@angular/core";
export declare class VitInputComponent {
    form: FormGroup;
    controlName: string;
    type: string;
    label: string;
    value: string | number;
    hint: string;
    disabled: boolean;
    constructor();
    static ɵfac: i0.ɵɵFactoryDef<VitInputComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<VitInputComponent, "vit-input", never, { "form": "form"; "controlName": "controlName"; "type": "type"; "label": "label"; "value": "value"; "hint": "hint"; "disabled": "disabled"; }, {}, never, never>;
}
