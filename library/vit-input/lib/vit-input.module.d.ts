import * as i0 from "@angular/core";
import * as i1 from "./vit-input";
import * as i2 from "@angular/common";
import * as i3 from "@angular/forms";
import * as i4 from "@angular/material/form-field";
export declare class VitInputModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<VitInputModule, [typeof i1.VitInputComponent], [typeof i2.CommonModule, typeof i3.ReactiveFormsModule, typeof i4.MatFormFieldModule], [typeof i1.VitInputComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<VitInputModule>;
}
