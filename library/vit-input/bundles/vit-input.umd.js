(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/forms'), require('@angular/material/form-field'), require('@angular/common')) :
    typeof define === 'function' && define.amd ? define('vit-input', ['exports', '@angular/core', '@angular/forms', '@angular/material/form-field', '@angular/common'], factory) :
    (global = global || self, factory(global['vit-input'] = {}, global.ng.core, global.ng.forms, global.ng.material.formField, global.ng.common));
}(this, (function (exports, core, forms, formField, common) { 'use strict';

    function VitInputComponent_mat_hint_5_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelement"](0, "mat-hint", 5);
    } if (rf & 2) {
        var ctx_r5 = core["ɵɵnextContext"]();
        core["ɵɵproperty"]("innerHTML", ctx_r5.hint, core["ɵɵsanitizeHtml"]);
    } }
    function VitInputComponent_mat_error_6_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r6 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"]("", ctx_r6.type, " is required");
    } }
    function VitInputComponent_mat_error_7_Template(rf, ctx) { if (rf & 1) {
        core["ɵɵelementStart"](0, "mat-error");
        core["ɵɵtext"](1);
        core["ɵɵelementEnd"]();
    } if (rf & 2) {
        var ctx_r7 = core["ɵɵnextContext"]();
        core["ɵɵadvance"](1);
        core["ɵɵtextInterpolate1"]("Invalid ", ctx_r7.type, "");
    } }
    var VitInputComponent = /** @class */ (function () {
        function VitInputComponent() {
            // Default values
            this.form = this.form ? this.form : null;
            this.controlName = this.controlName ? this.controlName : null;
            this.type = this.type ? this.type : 'text';
            this.value = this.value ? this.value : null;
        }
        VitInputComponent.ɵfac = function VitInputComponent_Factory(t) { return new (t || VitInputComponent)(); };
        VitInputComponent.ɵcmp = core["ɵɵdefineComponent"]({ type: VitInputComponent, selectors: [["vit-input"]], inputs: { form: "form", controlName: "controlName", type: "type", label: "label", value: "value", hint: "hint", disabled: "disabled" }, decls: 8, vars: 9, consts: [[3, "formGroup"], ["matInput", "", 3, "type", "formControlName", "value", "disabled"], ["input", ""], [3, "innerHTML", 4, "ngIf"], [4, "ngIf"], [3, "innerHTML"]], template: function VitInputComponent_Template(rf, ctx) { if (rf & 1) {
                core["ɵɵelementStart"](0, "mat-form-field", 0);
                core["ɵɵelementStart"](1, "mat-label");
                core["ɵɵtext"](2);
                core["ɵɵelementEnd"]();
                core["ɵɵelement"](3, "input", 1, 2);
                core["ɵɵtemplate"](5, VitInputComponent_mat_hint_5_Template, 1, 1, "mat-hint", 3);
                core["ɵɵtemplate"](6, VitInputComponent_mat_error_6_Template, 2, 1, "mat-error", 4);
                core["ɵɵtemplate"](7, VitInputComponent_mat_error_7_Template, 2, 1, "mat-error", 4);
                core["ɵɵelementEnd"]();
            } if (rf & 2) {
                var _r4 = core["ɵɵreference"](4);
                core["ɵɵproperty"]("formGroup", ctx.form);
                core["ɵɵadvance"](2);
                core["ɵɵtextInterpolate"](ctx.label);
                core["ɵɵadvance"](1);
                core["ɵɵpropertyInterpolate"]("type", ctx.type);
                core["ɵɵpropertyInterpolate"]("formControlName", ctx.controlName);
                core["ɵɵproperty"]("value", ctx.value)("disabled", ctx.disabled);
                core["ɵɵadvance"](2);
                core["ɵɵproperty"]("ngIf", ctx.hint);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", !_r4.value);
                core["ɵɵadvance"](1);
                core["ɵɵproperty"]("ngIf", _r4.value);
            } }, directives: [formField.MatFormField, forms.NgControlStatusGroup, forms.FormGroupDirective, formField.MatLabel, forms.DefaultValueAccessor, forms.NgControlStatus, forms.FormControlName, common.NgIf, formField.MatHint, formField.MatError], styles: ["[_nghost-%COMP%] { width: 100%; } .mat-error[_ngcontent-%COMP%]:first-letter { text-transform: uppercase; }"] });
        return VitInputComponent;
    }());
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](VitInputComponent, [{
            type: core.Component,
            args: [{
                    selector: 'vit-input',
                    templateUrl: './vit-input.html',
                    styles: [':host { width: 100%; } .mat-error:first-letter { text-transform: uppercase; }']
                }]
        }], function () { return []; }, { form: [{
                type: core.Input
            }], controlName: [{
                type: core.Input
            }], type: [{
                type: core.Input
            }], label: [{
                type: core.Input
            }], value: [{
                type: core.Input
            }], hint: [{
                type: core.Input
            }], disabled: [{
                type: core.Input
            }] }); })();

    var VitInputModule = /** @class */ (function () {
        function VitInputModule() {
        }
        VitInputModule.ɵmod = core["ɵɵdefineNgModule"]({ type: VitInputModule });
        VitInputModule.ɵinj = core["ɵɵdefineInjector"]({ factory: function VitInputModule_Factory(t) { return new (t || VitInputModule)(); }, imports: [[common.CommonModule, forms.ReactiveFormsModule, formField.MatFormFieldModule]] });
        return VitInputModule;
    }());
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && core["ɵɵsetNgModuleScope"](VitInputModule, { declarations: [VitInputComponent], imports: [common.CommonModule, forms.ReactiveFormsModule, formField.MatFormFieldModule], exports: [VitInputComponent] }); })();
    /*@__PURE__*/ (function () { core["ɵsetClassMetadata"](VitInputModule, [{
            type: core.NgModule,
            args: [{
                    declarations: [VitInputComponent],
                    imports: [common.CommonModule, forms.ReactiveFormsModule, formField.MatFormFieldModule],
                    exports: [VitInputComponent]
                }]
        }], null, null); })();

    exports.VitInputComponent = VitInputComponent;
    exports.VitInputModule = VitInputModule;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=vit-input.umd.js.map
