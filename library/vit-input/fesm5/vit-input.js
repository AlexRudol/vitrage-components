import { ɵɵelement, ɵɵnextContext, ɵɵproperty, ɵɵsanitizeHtml, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, ɵɵadvance, ɵɵtextInterpolate1, ɵɵdefineComponent, ɵɵtemplate, ɵɵreference, ɵɵtextInterpolate, ɵɵpropertyInterpolate, ɵsetClassMetadata, Component, Input, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { NgControlStatusGroup, FormGroupDirective, DefaultValueAccessor, NgControlStatus, FormControlName, ReactiveFormsModule } from '@angular/forms';
import { MatFormField, MatLabel, MatHint, MatError, MatFormFieldModule } from '@angular/material/form-field';
import { NgIf, CommonModule } from '@angular/common';

function VitInputComponent_mat_hint_5_Template(rf, ctx) { if (rf & 1) {
    ɵɵelement(0, "mat-hint", 5);
} if (rf & 2) {
    var ctx_r5 = ɵɵnextContext();
    ɵɵproperty("innerHTML", ctx_r5.hint, ɵɵsanitizeHtml);
} }
function VitInputComponent_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("", ctx_r6.type, " is required");
} }
function VitInputComponent_mat_error_7_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "mat-error");
    ɵɵtext(1);
    ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = ɵɵnextContext();
    ɵɵadvance(1);
    ɵɵtextInterpolate1("Invalid ", ctx_r7.type, "");
} }
var VitInputComponent = /** @class */ (function () {
    function VitInputComponent() {
        // Default values
        this.form = this.form ? this.form : null;
        this.controlName = this.controlName ? this.controlName : null;
        this.type = this.type ? this.type : 'text';
        this.value = this.value ? this.value : null;
    }
    VitInputComponent.ɵfac = function VitInputComponent_Factory(t) { return new (t || VitInputComponent)(); };
    VitInputComponent.ɵcmp = ɵɵdefineComponent({ type: VitInputComponent, selectors: [["vit-input"]], inputs: { form: "form", controlName: "controlName", type: "type", label: "label", value: "value", hint: "hint", disabled: "disabled" }, decls: 8, vars: 9, consts: [[3, "formGroup"], ["matInput", "", 3, "type", "formControlName", "value", "disabled"], ["input", ""], [3, "innerHTML", 4, "ngIf"], [4, "ngIf"], [3, "innerHTML"]], template: function VitInputComponent_Template(rf, ctx) { if (rf & 1) {
            ɵɵelementStart(0, "mat-form-field", 0);
            ɵɵelementStart(1, "mat-label");
            ɵɵtext(2);
            ɵɵelementEnd();
            ɵɵelement(3, "input", 1, 2);
            ɵɵtemplate(5, VitInputComponent_mat_hint_5_Template, 1, 1, "mat-hint", 3);
            ɵɵtemplate(6, VitInputComponent_mat_error_6_Template, 2, 1, "mat-error", 4);
            ɵɵtemplate(7, VitInputComponent_mat_error_7_Template, 2, 1, "mat-error", 4);
            ɵɵelementEnd();
        } if (rf & 2) {
            var _r4 = ɵɵreference(4);
            ɵɵproperty("formGroup", ctx.form);
            ɵɵadvance(2);
            ɵɵtextInterpolate(ctx.label);
            ɵɵadvance(1);
            ɵɵpropertyInterpolate("type", ctx.type);
            ɵɵpropertyInterpolate("formControlName", ctx.controlName);
            ɵɵproperty("value", ctx.value)("disabled", ctx.disabled);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.hint);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", !_r4.value);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", _r4.value);
        } }, directives: [MatFormField, NgControlStatusGroup, FormGroupDirective, MatLabel, DefaultValueAccessor, NgControlStatus, FormControlName, NgIf, MatHint, MatError], styles: ["[_nghost-%COMP%] { width: 100%; } .mat-error[_ngcontent-%COMP%]:first-letter { text-transform: uppercase; }"] });
    return VitInputComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(VitInputComponent, [{
        type: Component,
        args: [{
                selector: 'vit-input',
                templateUrl: './vit-input.html',
                styles: [':host { width: 100%; } .mat-error:first-letter { text-transform: uppercase; }']
            }]
    }], function () { return []; }, { form: [{
            type: Input
        }], controlName: [{
            type: Input
        }], type: [{
            type: Input
        }], label: [{
            type: Input
        }], value: [{
            type: Input
        }], hint: [{
            type: Input
        }], disabled: [{
            type: Input
        }] }); })();

var VitInputModule = /** @class */ (function () {
    function VitInputModule() {
    }
    VitInputModule.ɵmod = ɵɵdefineNgModule({ type: VitInputModule });
    VitInputModule.ɵinj = ɵɵdefineInjector({ factory: function VitInputModule_Factory(t) { return new (t || VitInputModule)(); }, imports: [[CommonModule, ReactiveFormsModule, MatFormFieldModule]] });
    return VitInputModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(VitInputModule, { declarations: [VitInputComponent], imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule], exports: [VitInputComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(VitInputModule, [{
        type: NgModule,
        args: [{
                declarations: [VitInputComponent],
                imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule],
                exports: [VitInputComponent]
            }]
    }], null, null); })();

/**
 * Generated bundle index. Do not edit.
 */

export { VitInputComponent, VitInputModule };
//# sourceMappingURL=vit-input.js.map
