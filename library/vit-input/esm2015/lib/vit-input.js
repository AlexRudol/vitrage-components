import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
function VitInputComponent_mat_hint_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-hint", 5);
} if (rf & 2) {
    const ctx_r1 = i0.ɵɵnextContext();
    i0.ɵɵproperty("innerHTML", ctx_r1.hint, i0.ɵɵsanitizeHtml);
} }
function VitInputComponent_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r2 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r2.type, " is required");
} }
function VitInputComponent_mat_error_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    const ctx_r3 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("Invalid ", ctx_r3.type, "");
} }
export class VitInputComponent {
    constructor() {
        // Default values
        this.form = this.form ? this.form : null;
        this.controlName = this.controlName ? this.controlName : null;
        this.type = this.type ? this.type : 'text';
        this.value = this.value ? this.value : null;
    }
}
VitInputComponent.ɵfac = function VitInputComponent_Factory(t) { return new (t || VitInputComponent)(); };
VitInputComponent.ɵcmp = i0.ɵɵdefineComponent({ type: VitInputComponent, selectors: [["vit-input"]], inputs: { form: "form", controlName: "controlName", type: "type", label: "label", value: "value", hint: "hint", disabled: "disabled" }, decls: 8, vars: 9, consts: [[3, "formGroup"], ["matInput", "", 3, "type", "formControlName", "value", "disabled"], ["input", ""], [3, "innerHTML", 4, "ngIf"], [4, "ngIf"], [3, "innerHTML"]], template: function VitInputComponent_Template(rf, ctx) { if (rf & 1) {
        i0.ɵɵelementStart(0, "mat-form-field", 0);
        i0.ɵɵelementStart(1, "mat-label");
        i0.ɵɵtext(2);
        i0.ɵɵelementEnd();
        i0.ɵɵelement(3, "input", 1, 2);
        i0.ɵɵtemplate(5, VitInputComponent_mat_hint_5_Template, 1, 1, "mat-hint", 3);
        i0.ɵɵtemplate(6, VitInputComponent_mat_error_6_Template, 2, 1, "mat-error", 4);
        i0.ɵɵtemplate(7, VitInputComponent_mat_error_7_Template, 2, 1, "mat-error", 4);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        const _r0 = i0.ɵɵreference(4);
        i0.ɵɵproperty("formGroup", ctx.form);
        i0.ɵɵadvance(2);
        i0.ɵɵtextInterpolate(ctx.label);
        i0.ɵɵadvance(1);
        i0.ɵɵpropertyInterpolate("type", ctx.type);
        i0.ɵɵpropertyInterpolate("formControlName", ctx.controlName);
        i0.ɵɵproperty("value", ctx.value)("disabled", ctx.disabled);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.hint);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", !_r0.value);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", _r0.value);
    } }, directives: [i1.MatFormField, i2.NgControlStatusGroup, i2.FormGroupDirective, i1.MatLabel, i2.DefaultValueAccessor, i2.NgControlStatus, i2.FormControlName, i3.NgIf, i1.MatHint, i1.MatError], styles: ["[_nghost-%COMP%] { width: 100%; } .mat-error[_ngcontent-%COMP%]:first-letter { text-transform: uppercase; }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitInputComponent, [{
        type: Component,
        args: [{
                selector: 'vit-input',
                templateUrl: './vit-input.html',
                styles: [':host { width: 100%; } .mat-error:first-letter { text-transform: uppercase; }']
            }]
    }], function () { return []; }, { form: [{
            type: Input
        }], controlName: [{
            type: Input
        }], type: [{
            type: Input
        }], label: [{
            type: Input
        }], value: [{
            type: Input
        }], hint: [{
            type: Input
        }], disabled: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWlucHV0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdml0LWlucHV0LyIsInNvdXJjZXMiOlsibGliL3ZpdC1pbnB1dC50cyIsImxpYi92aXQtaW5wdXQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7OztJQ1F2Qyw4QkFBc0Q7OztJQUEvQiwwREFBa0I7OztJQUd6QyxpQ0FBaUM7SUFBQSxZQUFvQjtJQUFBLGlCQUFZOzs7SUFBaEMsZUFBb0I7SUFBcEIsc0RBQW9COzs7SUFDckQsaUNBQWdDO0lBQUEsWUFBZ0I7SUFBQSxpQkFBWTs7O0lBQTVCLGVBQWdCO0lBQWhCLGtEQUFnQjs7QURMcEQsTUFBTSxPQUFPLGlCQUFpQjtJQVUxQjtRQUNJLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUM5RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUMzQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNoRCxDQUFDOztrRkFoQlEsaUJBQWlCO3NEQUFqQixpQkFBaUI7UUNSOUIseUNBRUk7UUFDQSxpQ0FBVztRQUFBLFlBQVM7UUFBQSxpQkFBWTtRQUdoQyw4QkFFQTtRQUNBLDRFQUEyQztRQUczQyw4RUFBaUM7UUFDakMsOEVBQWdDO1FBRXBDLGlCQUFpQjs7O1FBZkQsb0NBQWtCO1FBR25CLGVBQVM7UUFBVCwrQkFBUztRQUdHLGVBQWU7UUFBZiwwQ0FBZTtRQUFDLDREQUFpQztRQUFDLGlDQUFlLDBCQUFBO1FBRzlFLGVBQVk7UUFBWiwrQkFBWTtRQUdYLGVBQW9CO1FBQXBCLGlDQUFvQjtRQUNwQixlQUFtQjtRQUFuQixnQ0FBbUI7O2tERExyQixpQkFBaUI7Y0FMN0IsU0FBUztlQUFDO2dCQUNQLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixXQUFXLEVBQUUsa0JBQWtCO2dCQUMvQixNQUFNLEVBQUUsQ0FBQywrRUFBK0UsQ0FBQzthQUM1Rjs7a0JBR0ksS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSzs7a0JBQ0wsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1Hcm91cCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICd2aXQtaW5wdXQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi92aXQtaW5wdXQuaHRtbCcsXG4gICAgc3R5bGVzOiBbJzpob3N0IHsgd2lkdGg6IDEwMCU7IH0gLm1hdC1lcnJvcjpmaXJzdC1sZXR0ZXIgeyB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlOyB9J11cbn0pXG5leHBvcnQgY2xhc3MgVml0SW5wdXRDb21wb25lbnQge1xuXG4gICAgQElucHV0KCkgZm9ybTogRm9ybUdyb3VwOyAgICAgICAvLyBQYXJlbnQgZm9ybVxuICAgIEBJbnB1dCgpIGNvbnRyb2xOYW1lOiBzdHJpbmc7ICAgLy8gQ29udHJvbCBuYW1lIG9mIHRoZSBGb3JtIEdyb3VwXG4gICAgQElucHV0KCkgdHlwZTogc3RyaW5nOyAgICAgICAgICAvLyB0ZXh0IHx8IGVtYWlsIHx8IHBhc3N3b3JkIHx8IG51bWJlclxuICAgIEBJbnB1dCgpIGxhYmVsOiBzdHJpbmc7ICAgICAgICAgLy8gUGxhY2Vob2xkZXIgYW5kIGZpZWxkJ3MgbGFiZWxcbiAgICBASW5wdXQoKSB2YWx1ZTogc3RyaW5nIHwgbnVtYmVyOyAvLyBEZWZpbmVkIGlucHV0IHZhbHVlXG4gICAgQElucHV0KCkgaGludDogc3RyaW5nOyAgICAgICAgICAvLyBUZXh0IHVuZGVyIHRoZSBpbnB1dCwgc3VwcG9ydHMgSFRNTFxuICAgIEBJbnB1dCgpIGRpc2FibGVkOiBib29sZWFuOyAgICAgLy8gVGhlIGlucHV0IGlzIGRpc2FibGVkXG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgLy8gRGVmYXVsdCB2YWx1ZXNcbiAgICAgICAgdGhpcy5mb3JtID0gdGhpcy5mb3JtID8gdGhpcy5mb3JtIDogbnVsbDtcbiAgICAgICAgdGhpcy5jb250cm9sTmFtZSA9IHRoaXMuY29udHJvbE5hbWUgPyB0aGlzLmNvbnRyb2xOYW1lIDogbnVsbDtcbiAgICAgICAgdGhpcy50eXBlID0gdGhpcy50eXBlID8gdGhpcy50eXBlIDogJ3RleHQnO1xuICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy52YWx1ZSA/IHRoaXMudmFsdWUgOiBudWxsO1xuICAgIH1cblxufVxuIiwiPG1hdC1mb3JtLWZpZWxkIFtmb3JtR3JvdXBdPVwiZm9ybVwiID5cblxuICAgIDwhLS0gTGFiZWwgLS0+XG4gICAgPG1hdC1sYWJlbD57e2xhYmVsfX08L21hdC1sYWJlbD5cblxuICAgIDwhLS0gSW5wdXQgLS0+XG4gICAgPGlucHV0ICNpbnB1dCBtYXRJbnB1dCB0eXBlPVwie3t0eXBlfX1cIiBmb3JtQ29udHJvbE5hbWU9XCJ7e2NvbnRyb2xOYW1lfX1cIiBbdmFsdWVdPVwidmFsdWVcIiBbZGlzYWJsZWRdPVwiZGlzYWJsZWRcIiA+XG5cbiAgICA8IS0tIEhpbnQgLS0+XG4gICAgPG1hdC1oaW50ICpuZ0lmPVwiaGludFwiIFtpbm5lckhUTUxdPVwiaGludFwiID48L21hdC1oaW50PlxuXG4gICAgPCEtLSBFcnJvcnMgLS0+XG4gICAgPG1hdC1lcnJvciAqbmdJZj1cIiFpbnB1dC52YWx1ZVwiID57e3R5cGV9fSBpcyByZXF1aXJlZDwvbWF0LWVycm9yPlxuICAgIDxtYXQtZXJyb3IgKm5nSWY9XCJpbnB1dC52YWx1ZVwiID5JbnZhbGlkIHt7dHlwZX19PC9tYXQtZXJyb3I+XG5cbjwvbWF0LWZvcm0tZmllbGQ+XG4iXX0=