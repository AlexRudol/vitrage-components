import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitInputComponent } from './vit-input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import * as i0 from "@angular/core";
export class VitInputModule {
}
VitInputModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitInputModule });
VitInputModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitInputModule_Factory(t) { return new (t || VitInputModule)(); }, imports: [[CommonModule, ReactiveFormsModule, MatFormFieldModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitInputModule, { declarations: [VitInputComponent], imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule], exports: [VitInputComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitInputModule, [{
        type: NgModule,
        args: [{
                declarations: [VitInputComponent],
                imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule],
                exports: [VitInputComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWlucHV0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpdC1pbnB1dC8iLCJzb3VyY2VzIjpbImxpYi92aXQtaW5wdXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQzs7QUFPbEUsTUFBTSxPQUFPLGNBQWM7O2tEQUFkLGNBQWM7MkdBQWQsY0FBYyxrQkFIZCxDQUFFLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxrQkFBa0IsQ0FBRTt3RkFHekQsY0FBYyxtQkFKUCxpQkFBaUIsYUFDdEIsWUFBWSxFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixhQUNyRCxpQkFBaUI7a0RBRW5CLGNBQWM7Y0FMMUIsUUFBUTtlQUFDO2dCQUNOLFlBQVksRUFBRSxDQUFFLGlCQUFpQixDQUFFO2dCQUNuQyxPQUFPLEVBQUUsQ0FBRSxZQUFZLEVBQUUsbUJBQW1CLEVBQUUsa0JBQWtCLENBQUU7Z0JBQ2xFLE9BQU8sRUFBRSxDQUFFLGlCQUFpQixDQUFFO2FBQ2pDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBWaXRJbnB1dENvbXBvbmVudCB9IGZyb20gJy4vdml0LWlucHV0JztcbmltcG9ydCB7IFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBNYXRGb3JtRmllbGRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9mb3JtLWZpZWxkJztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFsgVml0SW5wdXRDb21wb25lbnQgXSxcbiAgICBpbXBvcnRzOiBbIENvbW1vbk1vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSwgTWF0Rm9ybUZpZWxkTW9kdWxlIF0sXG4gICAgZXhwb3J0czogWyBWaXRJbnB1dENvbXBvbmVudCBdXG59KVxuZXhwb3J0IGNsYXNzIFZpdElucHV0TW9kdWxlIHsgfVxuIl19