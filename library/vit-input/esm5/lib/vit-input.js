import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/form-field";
import * as i2 from "@angular/forms";
import * as i3 from "@angular/common";
function VitInputComponent_mat_hint_5_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelement(0, "mat-hint", 5);
} if (rf & 2) {
    var ctx_r5 = i0.ɵɵnextContext();
    i0.ɵɵproperty("innerHTML", ctx_r5.hint, i0.ɵɵsanitizeHtml);
} }
function VitInputComponent_mat_error_6_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r6 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("", ctx_r6.type, " is required");
} }
function VitInputComponent_mat_error_7_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "mat-error");
    i0.ɵɵtext(1);
    i0.ɵɵelementEnd();
} if (rf & 2) {
    var ctx_r7 = i0.ɵɵnextContext();
    i0.ɵɵadvance(1);
    i0.ɵɵtextInterpolate1("Invalid ", ctx_r7.type, "");
} }
var VitInputComponent = /** @class */ (function () {
    function VitInputComponent() {
        // Default values
        this.form = this.form ? this.form : null;
        this.controlName = this.controlName ? this.controlName : null;
        this.type = this.type ? this.type : 'text';
        this.value = this.value ? this.value : null;
    }
    VitInputComponent.ɵfac = function VitInputComponent_Factory(t) { return new (t || VitInputComponent)(); };
    VitInputComponent.ɵcmp = i0.ɵɵdefineComponent({ type: VitInputComponent, selectors: [["vit-input"]], inputs: { form: "form", controlName: "controlName", type: "type", label: "label", value: "value", hint: "hint", disabled: "disabled" }, decls: 8, vars: 9, consts: [[3, "formGroup"], ["matInput", "", 3, "type", "formControlName", "value", "disabled"], ["input", ""], [3, "innerHTML", 4, "ngIf"], [4, "ngIf"], [3, "innerHTML"]], template: function VitInputComponent_Template(rf, ctx) { if (rf & 1) {
            i0.ɵɵelementStart(0, "mat-form-field", 0);
            i0.ɵɵelementStart(1, "mat-label");
            i0.ɵɵtext(2);
            i0.ɵɵelementEnd();
            i0.ɵɵelement(3, "input", 1, 2);
            i0.ɵɵtemplate(5, VitInputComponent_mat_hint_5_Template, 1, 1, "mat-hint", 3);
            i0.ɵɵtemplate(6, VitInputComponent_mat_error_6_Template, 2, 1, "mat-error", 4);
            i0.ɵɵtemplate(7, VitInputComponent_mat_error_7_Template, 2, 1, "mat-error", 4);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            var _r4 = i0.ɵɵreference(4);
            i0.ɵɵproperty("formGroup", ctx.form);
            i0.ɵɵadvance(2);
            i0.ɵɵtextInterpolate(ctx.label);
            i0.ɵɵadvance(1);
            i0.ɵɵpropertyInterpolate("type", ctx.type);
            i0.ɵɵpropertyInterpolate("formControlName", ctx.controlName);
            i0.ɵɵproperty("value", ctx.value)("disabled", ctx.disabled);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.hint);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", !_r4.value);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", _r4.value);
        } }, directives: [i1.MatFormField, i2.NgControlStatusGroup, i2.FormGroupDirective, i1.MatLabel, i2.DefaultValueAccessor, i2.NgControlStatus, i2.FormControlName, i3.NgIf, i1.MatHint, i1.MatError], styles: ["[_nghost-%COMP%] { width: 100%; } .mat-error[_ngcontent-%COMP%]:first-letter { text-transform: uppercase; }"] });
    return VitInputComponent;
}());
export { VitInputComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitInputComponent, [{
        type: Component,
        args: [{
                selector: 'vit-input',
                templateUrl: './vit-input.html',
                styles: [':host { width: 100%; } .mat-error:first-letter { text-transform: uppercase; }']
            }]
    }], function () { return []; }, { form: [{
            type: Input
        }], controlName: [{
            type: Input
        }], type: [{
            type: Input
        }], label: [{
            type: Input
        }], value: [{
            type: Input
        }], hint: [{
            type: Input
        }], disabled: [{
            type: Input
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWlucHV0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdml0LWlucHV0LyIsInNvdXJjZXMiOlsibGliL3ZpdC1pbnB1dC50cyIsImxpYi92aXQtaW5wdXQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7OztJQ1F2Qyw4QkFBc0Q7OztJQUEvQiwwREFBa0I7OztJQUd6QyxpQ0FBaUM7SUFBQSxZQUFvQjtJQUFBLGlCQUFZOzs7SUFBaEMsZUFBb0I7SUFBcEIsc0RBQW9COzs7SUFDckQsaUNBQWdDO0lBQUEsWUFBZ0I7SUFBQSxpQkFBWTs7O0lBQTVCLGVBQWdCO0lBQWhCLGtEQUFnQjs7QURWcEQ7SUFlSTtRQUNJLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUM5RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUMzQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNoRCxDQUFDO3NGQWhCUSxpQkFBaUI7MERBQWpCLGlCQUFpQjtZQ1I5Qix5Q0FFSTtZQUNBLGlDQUFXO1lBQUEsWUFBUztZQUFBLGlCQUFZO1lBR2hDLDhCQUVBO1lBQ0EsNEVBQTJDO1lBRzNDLDhFQUFpQztZQUNqQyw4RUFBZ0M7WUFFcEMsaUJBQWlCOzs7WUFmRCxvQ0FBa0I7WUFHbkIsZUFBUztZQUFULCtCQUFTO1lBR0csZUFBZTtZQUFmLDBDQUFlO1lBQUMsNERBQWlDO1lBQUMsaUNBQWUsMEJBQUE7WUFHOUUsZUFBWTtZQUFaLCtCQUFZO1lBR1gsZUFBb0I7WUFBcEIsaUNBQW9CO1lBQ3BCLGVBQW1CO1lBQW5CLGdDQUFtQjs7NEJEYmxDO0NBMEJDLEFBdkJELElBdUJDO1NBbEJZLGlCQUFpQjtrREFBakIsaUJBQWlCO2NBTDdCLFNBQVM7ZUFBQztnQkFDUCxRQUFRLEVBQUUsV0FBVztnQkFDckIsV0FBVyxFQUFFLGtCQUFrQjtnQkFDL0IsTUFBTSxFQUFFLENBQUMsK0VBQStFLENBQUM7YUFDNUY7O2tCQUdJLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUs7O2tCQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndml0LWlucHV0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdml0LWlucHV0Lmh0bWwnLFxuICAgIHN0eWxlczogWyc6aG9zdCB7IHdpZHRoOiAxMDAlOyB9IC5tYXQtZXJyb3I6Zmlyc3QtbGV0dGVyIHsgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTsgfSddXG59KVxuZXhwb3J0IGNsYXNzIFZpdElucHV0Q29tcG9uZW50IHtcblxuICAgIEBJbnB1dCgpIGZvcm06IEZvcm1Hcm91cDsgICAgICAgLy8gUGFyZW50IGZvcm1cbiAgICBASW5wdXQoKSBjb250cm9sTmFtZTogc3RyaW5nOyAgIC8vIENvbnRyb2wgbmFtZSBvZiB0aGUgRm9ybSBHcm91cFxuICAgIEBJbnB1dCgpIHR5cGU6IHN0cmluZzsgICAgICAgICAgLy8gdGV4dCB8fCBlbWFpbCB8fCBwYXNzd29yZCB8fCBudW1iZXJcbiAgICBASW5wdXQoKSBsYWJlbDogc3RyaW5nOyAgICAgICAgIC8vIFBsYWNlaG9sZGVyIGFuZCBmaWVsZCdzIGxhYmVsXG4gICAgQElucHV0KCkgdmFsdWU6IHN0cmluZyB8IG51bWJlcjsgLy8gRGVmaW5lZCBpbnB1dCB2YWx1ZVxuICAgIEBJbnB1dCgpIGhpbnQ6IHN0cmluZzsgICAgICAgICAgLy8gVGV4dCB1bmRlciB0aGUgaW5wdXQsIHN1cHBvcnRzIEhUTUxcbiAgICBASW5wdXQoKSBkaXNhYmxlZDogYm9vbGVhbjsgICAgIC8vIFRoZSBpbnB1dCBpcyBkaXNhYmxlZFxuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIC8vIERlZmF1bHQgdmFsdWVzXG4gICAgICAgIHRoaXMuZm9ybSA9IHRoaXMuZm9ybSA/IHRoaXMuZm9ybSA6IG51bGw7XG4gICAgICAgIHRoaXMuY29udHJvbE5hbWUgPSB0aGlzLmNvbnRyb2xOYW1lID8gdGhpcy5jb250cm9sTmFtZSA6IG51bGw7XG4gICAgICAgIHRoaXMudHlwZSA9IHRoaXMudHlwZSA/IHRoaXMudHlwZSA6ICd0ZXh0JztcbiAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMudmFsdWUgPyB0aGlzLnZhbHVlIDogbnVsbDtcbiAgICB9XG5cbn1cbiIsIjxtYXQtZm9ybS1maWVsZCBbZm9ybUdyb3VwXT1cImZvcm1cIiA+XG5cbiAgICA8IS0tIExhYmVsIC0tPlxuICAgIDxtYXQtbGFiZWw+e3tsYWJlbH19PC9tYXQtbGFiZWw+XG5cbiAgICA8IS0tIElucHV0IC0tPlxuICAgIDxpbnB1dCAjaW5wdXQgbWF0SW5wdXQgdHlwZT1cInt7dHlwZX19XCIgZm9ybUNvbnRyb2xOYW1lPVwie3tjb250cm9sTmFtZX19XCIgW3ZhbHVlXT1cInZhbHVlXCIgW2Rpc2FibGVkXT1cImRpc2FibGVkXCIgPlxuXG4gICAgPCEtLSBIaW50IC0tPlxuICAgIDxtYXQtaGludCAqbmdJZj1cImhpbnRcIiBbaW5uZXJIVE1MXT1cImhpbnRcIiA+PC9tYXQtaGludD5cblxuICAgIDwhLS0gRXJyb3JzIC0tPlxuICAgIDxtYXQtZXJyb3IgKm5nSWY9XCIhaW5wdXQudmFsdWVcIiA+e3t0eXBlfX0gaXMgcmVxdWlyZWQ8L21hdC1lcnJvcj5cbiAgICA8bWF0LWVycm9yICpuZ0lmPVwiaW5wdXQudmFsdWVcIiA+SW52YWxpZCB7e3R5cGV9fTwvbWF0LWVycm9yPlxuXG48L21hdC1mb3JtLWZpZWxkPlxuIl19