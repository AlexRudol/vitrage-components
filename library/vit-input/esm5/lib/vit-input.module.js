import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitInputComponent } from './vit-input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import * as i0 from "@angular/core";
var VitInputModule = /** @class */ (function () {
    function VitInputModule() {
    }
    VitInputModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitInputModule });
    VitInputModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitInputModule_Factory(t) { return new (t || VitInputModule)(); }, imports: [[CommonModule, ReactiveFormsModule, MatFormFieldModule]] });
    return VitInputModule;
}());
export { VitInputModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitInputModule, { declarations: [VitInputComponent], imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule], exports: [VitInputComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitInputModule, [{
        type: NgModule,
        args: [{
                declarations: [VitInputComponent],
                imports: [CommonModule, ReactiveFormsModule, MatFormFieldModule],
                exports: [VitInputComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWlucHV0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3ZpdC1pbnB1dC8iLCJzb3VyY2VzIjpbImxpYi92aXQtaW5wdXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQzs7QUFFbEU7SUFBQTtLQUsrQjtzREFBbEIsY0FBYzsrR0FBZCxjQUFjLGtCQUhkLENBQUUsWUFBWSxFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixDQUFFO3lCQVJ0RTtDQVcrQixBQUwvQixJQUsrQjtTQUFsQixjQUFjO3dGQUFkLGNBQWMsbUJBSlAsaUJBQWlCLGFBQ3RCLFlBQVksRUFBRSxtQkFBbUIsRUFBRSxrQkFBa0IsYUFDckQsaUJBQWlCO2tEQUVuQixjQUFjO2NBTDFCLFFBQVE7ZUFBQztnQkFDTixZQUFZLEVBQUUsQ0FBRSxpQkFBaUIsQ0FBRTtnQkFDbkMsT0FBTyxFQUFFLENBQUUsWUFBWSxFQUFFLG1CQUFtQixFQUFFLGtCQUFrQixDQUFFO2dCQUNsRSxPQUFPLEVBQUUsQ0FBRSxpQkFBaUIsQ0FBRTthQUNqQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgVml0SW5wdXRDb21wb25lbnQgfSBmcm9tICcuL3ZpdC1pbnB1dCc7XG5pbXBvcnQgeyBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xuaW1wb3J0IHsgTWF0Rm9ybUZpZWxkTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvZm9ybS1maWVsZCc7XG5cbkBOZ01vZHVsZSh7XG4gICAgZGVjbGFyYXRpb25zOiBbIFZpdElucHV0Q29tcG9uZW50IF0sXG4gICAgaW1wb3J0czogWyBDb21tb25Nb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUsIE1hdEZvcm1GaWVsZE1vZHVsZSBdLFxuICAgIGV4cG9ydHM6IFsgVml0SW5wdXRDb21wb25lbnQgXVxufSlcbmV4cG9ydCBjbGFzcyBWaXRJbnB1dE1vZHVsZSB7IH1cbiJdfQ==