import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { VitLogoComponent } from './vit-logo';
import * as i0 from "@angular/core";
export class VitLogoModule {
}
VitLogoModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitLogoModule });
VitLogoModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitLogoModule_Factory(t) { return new (t || VitLogoModule)(); }, imports: [[CommonModule, MatButtonModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitLogoModule, { declarations: [VitLogoComponent], imports: [CommonModule, MatButtonModule], exports: [VitLogoComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitLogoModule, [{
        type: NgModule,
        args: [{
                declarations: [VitLogoComponent],
                imports: [CommonModule, MatButtonModule],
                exports: [VitLogoComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWxvZ28ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vY29tcG9uZW50cy92aXQtbG9nby9zcmMvbGliL3ZpdC1sb2dvLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sWUFBWSxDQUFDOztBQU85QyxNQUFNLE9BQU8sYUFBYTs7aURBQWIsYUFBYTt5R0FBYixhQUFhLGtCQUhiLENBQUUsWUFBWSxFQUFFLGVBQWUsQ0FBRTt3RkFHakMsYUFBYSxtQkFKTixnQkFBZ0IsYUFDckIsWUFBWSxFQUFFLGVBQWUsYUFDN0IsZ0JBQWdCO2tEQUVsQixhQUFhO2NBTHpCLFFBQVE7ZUFBQztnQkFDTixZQUFZLEVBQUUsQ0FBRSxnQkFBZ0IsQ0FBRTtnQkFDbEMsT0FBTyxFQUFFLENBQUUsWUFBWSxFQUFFLGVBQWUsQ0FBRTtnQkFDMUMsT0FBTyxFQUFFLENBQUUsZ0JBQWdCLENBQUU7YUFDaEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbic7XG5pbXBvcnQgeyBWaXRMb2dvQ29tcG9uZW50IH0gZnJvbSAnLi92aXQtbG9nbyc7XG5cbkBOZ01vZHVsZSh7XG4gICAgZGVjbGFyYXRpb25zOiBbIFZpdExvZ29Db21wb25lbnQgXSxcbiAgICBpbXBvcnRzOiBbIENvbW1vbk1vZHVsZSwgTWF0QnV0dG9uTW9kdWxlIF0sXG4gICAgZXhwb3J0czogWyBWaXRMb2dvQ29tcG9uZW50IF1cbn0pXG5leHBvcnQgY2xhc3MgVml0TG9nb01vZHVsZSB7IH1cbiJdfQ==