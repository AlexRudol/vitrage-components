import * as i0 from "@angular/core";
import * as i1 from "./vit-logo";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
export declare class VitLogoModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<VitLogoModule, [typeof i1.VitLogoComponent], [typeof i2.CommonModule, typeof i3.MatButtonModule], [typeof i1.VitLogoComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<VitLogoModule>;
}
