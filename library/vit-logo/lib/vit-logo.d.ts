import * as i0 from "@angular/core";
export declare class VitLogoComponent {
    image: string;
    title?: string;
    compact?: boolean;
    width?: number;
    height?: number;
    nightMode?: boolean;
    background?: string;
    static ɵfac: i0.ɵɵFactoryDef<VitLogoComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<VitLogoComponent, "vit-logo", never, { "image": "image"; "title": "title"; "compact": "compact"; "width": "width"; "height": "height"; "nightMode": "nightMode"; "background": "background"; }, {}, never, never>;
}
