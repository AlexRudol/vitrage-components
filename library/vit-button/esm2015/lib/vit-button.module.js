import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { VitButtonComponent } from './vit-button';
import * as i0 from "@angular/core";
export class VitButtonModule {
}
VitButtonModule.ɵmod = i0.ɵɵdefineNgModule({ type: VitButtonModule });
VitButtonModule.ɵinj = i0.ɵɵdefineInjector({ factory: function VitButtonModule_Factory(t) { return new (t || VitButtonModule)(); }, imports: [[CommonModule, MatButtonModule, MatIconModule]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(VitButtonModule, { declarations: [VitButtonComponent], imports: [CommonModule, MatButtonModule, MatIconModule], exports: [VitButtonComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(VitButtonModule, [{
        type: NgModule,
        args: [{
                declarations: [VitButtonComponent],
                imports: [CommonModule, MatButtonModule, MatIconModule],
                exports: [VitButtonComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidml0LWJ1dHRvbi5tb2R1bGUuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi9jb21wb25lbnRzL3ZpdC1idXR0b24vc3JjL2xpYi92aXQtYnV0dG9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGNBQWMsQ0FBQzs7QUFPbEQsTUFBTSxPQUFPLGVBQWU7O21EQUFmLGVBQWU7NkdBQWYsZUFBZSxrQkFIZixDQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsYUFBYSxDQUFFO3dGQUdoRCxlQUFlLG1CQUpSLGtCQUFrQixhQUN2QixZQUFZLEVBQUUsZUFBZSxFQUFFLGFBQWEsYUFDNUMsa0JBQWtCO2tEQUVwQixlQUFlO2NBTDNCLFFBQVE7ZUFBQztnQkFDTixZQUFZLEVBQUUsQ0FBRSxrQkFBa0IsQ0FBRTtnQkFDcEMsT0FBTyxFQUFFLENBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxhQUFhLENBQUU7Z0JBQ3pELE9BQU8sRUFBRSxDQUFFLGtCQUFrQixDQUFFO2FBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBNYXRCdXR0b25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9idXR0b24nO1xuaW1wb3J0IHsgTWF0SWNvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2ljb24nO1xuaW1wb3J0IHsgVml0QnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi92aXQtYnV0dG9uJztcblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFsgVml0QnV0dG9uQ29tcG9uZW50IF0sXG4gICAgaW1wb3J0czogWyBDb21tb25Nb2R1bGUsIE1hdEJ1dHRvbk1vZHVsZSwgTWF0SWNvbk1vZHVsZSBdLFxuICAgIGV4cG9ydHM6IFsgVml0QnV0dG9uQ29tcG9uZW50IF1cbn0pXG5leHBvcnQgY2xhc3MgVml0QnV0dG9uTW9kdWxlIHsgfVxuIl19