import * as i0 from "@angular/core";
import * as i1 from "./vit-button";
import * as i2 from "@angular/common";
import * as i3 from "@angular/material/button";
import * as i4 from "@angular/material/icon";
export declare class VitButtonModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<VitButtonModule, [typeof i1.VitButtonComponent], [typeof i2.CommonModule, typeof i3.MatButtonModule, typeof i4.MatIconModule], [typeof i1.VitButtonComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<VitButtonModule>;
}
