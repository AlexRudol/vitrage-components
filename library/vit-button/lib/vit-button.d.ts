import * as i0 from "@angular/core";
export declare enum ButtonType {
    default = "default",
    primary = "primary",
    lite = "lite"
}
export declare class VitButtonComponent {
    type?: ButtonType;
    compact?: boolean;
    wide?: boolean;
    label: string;
    icon?: string;
    iconRight?: boolean;
    tooltip?: string;
    disabled?: boolean;
    loading?: boolean;
    constructor();
    static ɵfac: i0.ɵɵFactoryDef<VitButtonComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<VitButtonComponent, "vit-button", never, { "type": "type"; "compact": "compact"; "wide": "wide"; "label": "label"; "icon": "icon"; "iconRight": "iconRight"; "tooltip": "tooltip"; "disabled": "disabled"; "loading": "loading"; }, {}, never, never>;
}
