![logo-vitrage](logo-vitrage.png)<br><br>

Themes, UI components and design guides for my projects.
It includes Documentation as well as the Components library.

Library of generic components (e.g. buttons, form inputs, menus) and composite components (e.g. login form, sidebar navigation, content page).

<details>
<summary>Storybook 5</summary>

| package | description |
| --- | --- |
| `@storybook/addon-backgrounds` | Switch between custom backgrounds in Storybook's canvas. |
| `@storybook/addon-docs` | MDX documentation for stories. |
| `@storybook/addon-jest` | Jest unit-tests coverage for stories. |
| `@storybook/addon-storysource` | Show the source code for a story. |
| `@storybook/addons` | Enable add-ons to Storybook. |
| `@storybook/angular` | Angular support. |
| `@storybook/cli` | CLI tool Storybook. |
| `storybook-addon-themes` | Switch between custom themes for stories. |
| `storybook-dark-mode` | Dark mode. |

</details>

<details>
<summary>TypeScript 3</summary>

| package | description |
| --- | --- |
| `typescript` | To compile TS into JS. |

</details>

<details>
<summary>Jest 26</summary>

| package | description |
| --- | --- |
| `@angular-builders/jest` | Angular's builder for Jest. |
| `jest` | Testing framework for TS and JS. |
| `@types/jest` | Type definitions for Jest. |
| `jest-preset-angular` | Jest config's presets for Angular. |
| `ts-jest` | TS preprocessor for Jest so tests can be written in TS. |
| `@ngneat/spectator` | A tool to simplify unit-tests for Angular. |

</details>

<details>
<summary>Styles</summary>

| package | description |
| --- | --- |
| `bulma` | Modular CSS framework. |

</details>


### Set up

Prerequirements: `node v14 || v15`

To install the dependencies just run: `npm ci`

### Use

| command | description |
| --- | --- |
| `npm start` | Run Storybook on local [http://localhost:1111](http://localhost:1111). |
| `npm run start:no-test` | Run Storybook with no tests' watcher. |
| `npm run test` | Run the unit-tests. |
| `npm run lint` | Run TypeScript linter. |
| `npm run build` | Build Storybook to the `/dist`. |
| `ng build {vit-component}` | Build a specific component where {vit-component} is the name of the component. It builds components from the `./components` and put the production ready compiled library's component into the `./library`. |

### Maintainers

- [Alex Rudol](https://alexrudol.com) – [me@alexrudol.com](mailto:me@alexrudol.com?subject=[GitLab]%20Contributing%Vitrage%20Components)

### License

[MIT](LICENSE.md)
