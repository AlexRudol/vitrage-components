## 0.8.5 (12 Jul 2020)

Huge update to the configs, unit-tests and stories.

### Components
* VitSidePanel component refactored.
* Created the VitMenu component.
* The VitMenu component included into the VitSidePanel component as an optional sub-component.
* Updated parameters and styles of the component.
* Removed unnecessary VitButton dependencies from both VitMenu and VitSidePanel.
* Better description in the code for both components.

### Stories
* Updated stories of the VitSidePanel.
* Created stories of the VitMenu.
* Added theming section for both VitMenu and VitSidePanel.

### Theme
* Added the custom Scrollbar class and parameters.


## 0.8.0 (2 Jun 2020)

Updated the Library description.

### Components
* Configured unit-tests for all library and separate components.
* Added unit-tests with the coverage 90% and up.

### Stories
* Updated the format of the Stories. Added Theming section.


## 0.7.0 (18 Apr 2020)

Added the project logo.

### Components
* Updated the SidePanelComponent.
* Many updates to the angular and typescript configs.
* Added Angular Library folder.

### Stories
* Correspondent updates to the SidePanel stories.


## 0.6.0 (5 Apr 2020)

### Components
* Added the SidePanelComponent.
* Updated the ButtonComponent.
* Many minor updates and hotfixes to the others.

### Stories
* Added the SidePanel stories.
* Many minor updates to the other stories.
* Updates the stories layout.


## 0.5.5 (14 Jan 2020)

### Dependencies
* Storybook is updated to the stable v5.3.

### Components
* Added the wide version of the ButtonComponent.
* A small update to the InputComponent.


## 0.5.0 (30 Dec 2019)

The project renamed to the Vitrage Components.

### Dependencies
* Added Bulma CSS framework.
* Minor updates to the other dependencies.

### Stories
* Added theme switch.
* Updated the custom theme.


## 0.4.0 (5 Dec 2019)

### Dependencies
* Updated the Storybook to the latest (still in Beta).
* Updated the corresponded dependencies.

### Components
* Added LoginFormComponent.
* Updated ButtonComponent.

### Stories
* Added the custom theme.
* Added Typography story.


## 0.3.0 (11 Nov 2019)

### Dependencies
* Huge upgrade of the Storybook (5 > 5.3) and the other dependencies.

### Components
* Updated the structure and inner dependencies.

### Stories
* Updated the syntax for all existing stories.
* Updated the way how the stories render.


## 0.2.0 (2 Aug 2019)

### Components
* Added Button Components.
* Added Logo component.

### Stories
* Added Form stories.


## 0.1.0 (11 Jul 2019)

The new version with Storybook 5 and Angular 8.
Created based on the Plasticine styles micro-framework (currently archived).
