import { addDecorator, addParameters, moduleMetadata } from '@storybook/angular';
import storybookTheme from './storybook-theme.js';
import { setCompodocJson } from '@storybook/addon-docs/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

// @ts-ignore
// eslint-disable-next-line import/extensions, import/no-unresolved
import docJson from '../documentation.json';
setCompodocJson(docJson);

addDecorator(
    moduleMetadata({
        imports: [ BrowserModule, BrowserAnimationsModule ]
    })
);

addParameters({
    docs: {
        iframeHeight: '70px'
    },
    viewMode: 'docs',
    options: {
        theme: storybookTheme
    },
    themes: [
        { name: 'Hegrim Main Theme', class: 'hegrim-main-theme', color: '#dd9e00' },
        { name: 'Hegrim Main Theme - Dark', class: 'hegrim-main-theme-dark', color: '#148e73' },
        { name: 'Hegrim Time Theme', class: 'hegrim-time-theme', color: '#d54e00' },
        { name: 'Hegrim Time Theme - Dark', class: 'hegrim-time-theme-dark', color: '#7600a8' },
        { name: 'Hegrim Notes Theme', class: 'hegrim-notes-theme', color: '#7ea800' },
        { name: 'Hegrim Notes Theme - Dark', class: 'hegrim-notes-theme-dark', color: '#ae2600' },
        { name: 'Hegrim Projects Theme', class: 'hegrim-projects-theme', color: '#0062a8', default: true },
        { name: 'Hegrim Projects Theme - Dark', class: 'hegrim-projects-theme-dark', color: '#ae00ae' }
    ],
});
