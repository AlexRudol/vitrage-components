import { create } from '@storybook/theming/create';

export default create({
    base: 'light',

    colorPrimary: '#1c3255',
    colorSecondary: '#eee',

    // UI
    appBg: '#fff',
    appContentBg: '#fff',
    appBorderColor: '#fff',
    appBorderRadius: 0,

    // Typography
    fontBase: 'Roboto, sans-serif',
    fontCode: 'monospace',

    // Text colors
    textColor: '#444',
    textInverseColor: 'rgba(255,255,255,0.9)',

    // Toolbar default and active colors
    barTextColor: '#bcbcbc',
    barSelectedColor: '#646464',
    barBg: '#f9f9f9',

    // Form colors
    inputBg: '#fff',
    inputBorder: '#bcbcbc',
    inputTextColor: '#444',
    inputBorderRadius: 0,

    brandTitle: 'Vitrage Components'
});
