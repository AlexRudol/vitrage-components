module.exports = {
    presets: ['@storybook/addon-docs/preset'],
    stories: [
        '../stories/documentation/about.stories.mdx',
        '../stories/**/*.stories.(ts|mdx)'
    ],
    addons: [
        '@storybook/addon-storysource/register',
        'storybook-addon-themes/register',
        '@storybook-dark-mode/register'
        // '@storybook/addon-jest/register'
    ]
};
