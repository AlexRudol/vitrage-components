import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitLoginFormComponent } from './vit-form-login';
import { VitInputModule } from '@library/vit-input';
import { ReactiveFormsModule } from '@angular/forms';
import { VitButtonModule } from '@library/vit-button';

@NgModule({
    declarations: [ VitLoginFormComponent ],
    imports: [ CommonModule, VitButtonModule, VitInputModule, ReactiveFormsModule ],
    exports: [ VitLoginFormComponent ]
})
export class VitLoginFormModule { }
