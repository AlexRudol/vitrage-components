import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'vit-form-login',
    templateUrl: './vit-form-login.html',
    styleUrls: ['vit-form-login.scss']
})
export class VitLoginFormComponent implements OnInit {

    formLogin: FormGroup;

    constructor() {
        /*
         * Specifying the form fields
         */
        this.formLogin = new FormGroup({
            email: new FormControl('', {
                validators: [ Validators.required, Validators.email ], updateOn: 'blur'
            }),
            password: new FormControl('', {
                validators: [ Validators.required ], updateOn: 'change'
            })
        });
    }

    /*
     * Reset the validation if the field is empty again
     */
    ngOnInit() {
        // Reset validation if the input is empty again
        this.formLogin.valueChanges
            .subscribe(() => {
                this.resetForm(this.formLogin);
            });
    }

    /*
     * Collect the form values and send it outside
     */
    getValues(): void {
        console.log(this.formLogin.value);
    }

    // TODO: Refactor (function as a helper?)
    resetForm(form: FormGroup) {
        Object.keys(form.controls).forEach( (key: string) => {
            if (form.get(key).value === '') {
                form.get(key).setErrors(null);
            }
        });
    }

}
