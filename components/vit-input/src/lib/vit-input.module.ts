import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitInputComponent } from './vit-input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';

@NgModule({
    declarations: [ VitInputComponent ],
    imports: [ CommonModule, ReactiveFormsModule, MatFormFieldModule ],
    exports: [ VitInputComponent ]
})
export class VitInputModule { }
