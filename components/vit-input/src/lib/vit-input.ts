import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'vit-input',
    templateUrl: './vit-input.html',
    styles: [':host { width: 100%; } .mat-error:first-letter { text-transform: uppercase; }']
})
export class VitInputComponent {

    @Input() form: FormGroup;       // Parent form
    @Input() controlName: string;   // Control name of the Form Group
    @Input() type: string;          // text || email || password || number
    @Input() label: string;         // Placeholder and field's label
    @Input() value: string | number; // Defined input value
    @Input() hint: string;          // Text under the input, supports HTML
    @Input() disabled: boolean;     // The input is disabled

    constructor() {
        // Default values
        this.form = this.form ? this.form : null;
        this.controlName = this.controlName ? this.controlName : null;
        this.type = this.type ? this.type : 'text';
        this.value = this.value ? this.value : null;
    }

}
