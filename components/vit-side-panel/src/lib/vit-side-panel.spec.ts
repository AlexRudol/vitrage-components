import { createComponentFactory, Spectator, SpectatorFactory } from '@ngneat/spectator/jest';
import { fakeAsync } from '@angular/core/testing';
import { VitSidePanelComponent } from './vit-side-panel';
import { VitMenuComponent } from '@components/vit-menu/src/lib/vit-menu';
import { MenuItemModel } from '@components/vit-menu/src/lib/menu-item.model';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

const navigationSource: MenuItemModel[] = require('../../../../stories/components/shared/menu.json');

describe('VitSidePanelComponent', () => {
    let spectator: Spectator<VitSidePanelComponent>;
    const createComponent: SpectatorFactory<VitSidePanelComponent> = createComponentFactory({
        component: VitSidePanelComponent,
        declarations: [ VitMenuComponent ],
        imports: [ MatButtonModule, MatIconModule, MatListModule ]
    });

    beforeEach(() => spectator = createComponent());

    it('should create the Side Panel', () => {
        expect(spectator).toBeTruthy();
    });


    // MODES

    // Compact
    it('has Compact Mode', () => {
        spectator.setInput('compactMode', true);
        expect(spectator.component.classes).toBe('compact has-toggle ');
    });
    it('has no Compact Mode if not defined and the window size is big enough', () => {
        expect(window.innerWidth).toEqual(1024);
        expect(spectator.component.classes).toBe('has-toggle ');
    });
    it('has no Compact Mode if message is defined and the window size is small', () => {
        spectator.setInput('message', 'Sample Side Panel message');
        global.innerWidth = 700;
        global.window.dispatchEvent(new Event('resize'));
        expect(global.window.innerWidth).toEqual(700);
        expect(spectator.component.classes).toBe('has-toggle ');
        global.innerWidth = 1024; // Set it back to the default value for the next tests
    });
    it('has Compact Mode if the window size is small', () => {
        global.innerWidth = 700;
        global.window.dispatchEvent(new Event('resize'));
        expect(global.window.innerWidth).toEqual(700);
        expect(spectator.component.classes).toBe('compact has-toggle ');
        global.innerWidth = 1024; // Set it back to the default value for the next tests
    });
    // Night
    it('has night class (nightMode)', () => {
        spectator.setInput('nightMode', true);
        expect(window.innerWidth).toEqual(1024);
        expect(spectator.component.classes).toBe('has-toggle night ');
    });


    // CONTENT

    // Toggle
    it('has Toggle enabled by default', () => {
        expect(spectator.query('#toggle')).toBeTruthy();
    });
    it('has no Toggle', () => {
        spectator.setInput('toggle', false);
        expect(spectator.query('#toggle')).toBeFalsy();
    });
    it('toggles the Panel Mode on click', fakeAsync(() => {
        expect(spectator.component.classes).toBe('has-toggle ');
        spectator.click('#toggle');
        expect(spectator.component.classes).toBe('compact has-toggle animated ');
        spectator.tick(500);
        expect(spectator.component.classes).toBe('compact has-toggle ');
    }));

    // Logo
    it('has no Logos if nothing specified', () => {
        expect(spectator.query('#logo')).toBeFalsy();
        expect(spectator.query('#logoCompact')).toBeFalsy();
    });
    // Compact mode
    it('has compact Logo in Compact Mode', () => {
        spectator.setInput('logoCompact', 'SL');
        expect(spectator.query('#logo .logo-full')).toBeFalsy();
        expect(spectator.query('#logo .logo-compact').innerHTML)
            .toBe('SL');
    });
    // Default mode
    it('has full Logo in Default Mode', () => {
        spectator.setInput('compactMode', false);
        spectator.setInput('logo', 'sample logo');
        expect(spectator.query('#logo .logo-compact')).toBeFalsy();
        expect(spectator.query('#logo .logo-full').innerHTML)
            .toBe('sample logo');
    });


    // Message
    it('has no Message if not defined', () => {
        spectator.setInput('compactMode', false);
        expect(spectator.query('#message')).toBeFalsy();
    });
    it('has no Message in Compact Mode', () => {
        spectator.setInput('compactMode', true);
        spectator.setInput('message', 'sample message');
        expect(spectator.query('#message')).toBeFalsy();
    });
    it('has custom Message', () => {
        spectator.setInput('compactMode', false);
        spectator.setInput('message', 'sample message');
        expect(spectator.query('#message').innerHTML)
            .toBe('sample message');
        expect(spectator.query('#menu')).toBeFalsy();
    });


    // Menu
    it('has Menu items', () => {
        spectator.setInput('navigation', navigationSource);
        expect(spectator.query('#message')).toBeFalsy();
        expect(spectator.query('#menu')).toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(1) .label').innerHTML)
            .toBe('Item 1');
        expect(spectator.query('.mat-list-item:nth-child(2) .label').innerHTML)
            .toBe('Item 2');
    });


    // Background content
    it('has Background content', () => {
        spectator.setInput('backgroundHTML', '<div class="custom back">interactive background</div>');
        expect(spectator.query('#background').innerHTML)
            .toBe('<div class="custom back">interactive background</div>');
    });


    // OUTPUT

    // Logo click
    it('trigger event on compact Logo click', () => {
        let output: string;
        spectator.setInput('logoCompact', 'SL');
        spectator.output('logoClickEvent').subscribe((result: string) => (output = result));
        spectator.click('#logo .logo-compact');
        expect(output).toEqual('compact');
    });
    // Panel toggle click
    it('trigger event on Panel Toggle click', () => {
        let isCompactMode: boolean;
        spectator.output('toggleClickEvent').subscribe((result: boolean) => (isCompactMode = result));
        spectator.click('#toggle');
        // As the Panel mode is not "compact" initially, by toggling isCompactMode becomes "true"
        expect(isCompactMode).toBeTruthy();
    });
    // Menu item
    it('trigger event on Menu item click', () => {
        let output: string[];
        spectator.setInput('navigation', navigationSource);
        spectator.output('selectMenuItemEvent').subscribe((result: string[]) => (output = result));
        spectator.click('.mat-list-item:nth-child(1)');
        expect(output).toEqual(['#link1', undefined]);
    });

});
