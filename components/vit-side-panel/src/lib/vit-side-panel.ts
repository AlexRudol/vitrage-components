import {
    Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output, ViewEncapsulation
} from '@angular/core';
import { MenuItemModel } from '@library/vit-menu/lib/menu-item.model';

export enum LogoMode {
    full = 'full',
    compact = 'compact'
}

@Component({
    selector: 'vit-side-panel',
    templateUrl: './vit-side-panel.html',
    styleUrls: ['./vit-side-panel.scss', './vit-side-panel.theme.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VitSidePanelComponent implements OnInit {

    // Modes
    @Input() compactMode?: boolean;         // Toggle the Panel compact mode
    @Input() nightMode?: boolean;           // Night mode of the Panel - switches colors

    // Content
    @Input() toggle?: boolean = true;           // Show or hide Panel toggle
    @Input() logo?: string;                     // Logo (html or text)
    @Input() logoCompact?: string;              // Compact logo (html or text) to show in compact Panel mode
    @Input() message?: string;                  // Show a static message in the Panel
    @Input() navigation?: MenuItemModel[];      // Navigation block (main menu)
    @Input() alwaysShowIcons?: boolean;         // Show icons in both compact and default modes
    @Input() expandMultiple?: boolean = false;  // Allow to expand multiple items
    @Input() rightAligned?: boolean = false;    // Align content inside to the right, send the same parameter to Menu
    @Input() backgroundHTML?: string;           // Background - html or image to add static or interactive background

    // Outputs
    @Output() selectMenuItemEvent: EventEmitter<Array<string>> = new EventEmitter();    // Menu item is selected
    @Output() logoClickEvent: EventEmitter<LogoMode> = new EventEmitter();              // User clicked the Logo
    @Output() toggleClickEvent: EventEmitter<boolean> = new EventEmitter();             // The panel is toggled

    logoMode: typeof LogoMode = LogoMode;   // Full or compact
    animatedPanel: boolean = false;         // Turn on/off the Panel animation

    // Apply host classes
    @HostBinding('class')
    get classes(): string {
        return (this.compactMode ? 'compact ' : '')
            + (this.toggle ? 'has-toggle ' : '')
            + (this.rightAligned ? 'right-aligned ' : '')
            + (this.animatedPanel ? 'animated ' : '')
            + (this.nightMode ? 'night ' : '');
    }

    // Toggle the Panel compact mode on window resize
    @HostListener('window:resize', ['$event'])
    onResize() {
        this.setPanelMode();
    }

    constructor() { }

    ngOnInit() {
        // The compactMode is on "auto" if not defined initially
        if (this.compactMode === undefined) {
            this.setPanelMode();
        }
    }

    /*
     * Set the Panel compact mode based on the current window size
     */
    setPanelMode() {
        if (!(this.message && !this.compactMode)) {
            this.compactMode = window.innerWidth < 1000;
        }
    }

    /*
     * Toggle the Panel mode between default and compact state
     */
    togglePanelMode() {
        this.animatedPanel = true;
        this.compactMode = !this.compactMode;
        setTimeout(() => { this.animatedPanel = false; }, 500);
    }
    clickOnToggle() {
        this.togglePanelMode();
        this.toggleClickEvent.emit(this.compactMode);
    }

    /*
     * Click on the Logo
     */
    clickOnLogo(logoMode: LogoMode) {
        this.logoClickEvent.emit(logoMode);
    }

    /*
     * Select menu item
     */
    selectMenuItem(selectedItem: string[]) {
        this.selectMenuItemEvent.emit(selectedItem);
    }

}
