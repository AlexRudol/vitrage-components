import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitSidePanelComponent } from './vit-side-panel';
import { VitMenuModule } from '@library/vit-menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { VitButtonModule } from '@library/vit-button';

@NgModule({
    declarations: [ VitSidePanelComponent ],
    imports: [CommonModule, VitMenuModule, MatButtonModule, MatIconModule, VitButtonModule],
    exports: [ VitSidePanelComponent ]
})
export class VitSidePanelModule { }
