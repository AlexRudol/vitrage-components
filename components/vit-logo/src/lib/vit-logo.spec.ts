import { createComponentFactory, Spectator, SpectatorFactory } from '@ngneat/spectator/jest';
import { VitLogoComponent } from '@components/vit-logo/src/lib/vit-logo';

describe('VitLogoComponent', () => {
    let spectator: Spectator<VitLogoComponent>;
    const createComponent: SpectatorFactory<VitLogoComponent> = createComponentFactory(VitLogoComponent);

    beforeEach(() => spectator = createComponent());

    it('should create the Logo', () => {
        expect(spectator).toBeTruthy();
    });

    // Image
    it('has Logo image', () => {
        spectator.setInput('image', '../../../../logo-vitrage.png');
        expect(spectator.query('button').innerHTML)
            .toBe('<img src="../../../../logo-vitrage.png" alt="Logo">');
    });

    // Title
    it('has default title', () => {
        expect(spectator.query('button').outerHTML)
            .toBe('<button mat-button="" title="Logo"><img src="" alt="Logo"></button>');
    });
    it('has custom title', () => {
        spectator.setInput('title', 'Custom');
        expect(spectator.query('button').outerHTML)
            .toBe('<button mat-button="" title="Custom"><img src="" alt="Custom"></button>');
    });

    // Compact
    it('has compact class (compact mode)', () => {
        spectator.setInput('compact', true);
        expect(spectator.query('button').classList[0])
            .toBe('compact');
    });

    // Custom size
    it('has custom width', () => {
        spectator.setInput('width', 128);
        expect(spectator.query('button').getAttribute('style'))
            .toMatch(/width: 128px; min-width: 128px; max-width: 128px;/);
    });
    it('has custom height', () => {
        spectator.setInput('height', 128);
        expect(spectator.query('button').getAttribute('style'))
            .toMatch(/height: 128px; min-height: 128px; max-height: 128px;/);
    });

    // Night mode
    it('has night class (nightMode)', () => {
        spectator.setInput('nightMode', true);
        expect(spectator.query('button').classList[0])
            .toBe('night');
    });

    // Background
    it('has custom background color', () => {
        spectator.setInput('background', '#444');
        expect(spectator.query('button').getAttribute('style'))
            .toMatch(/background-color: rgb\(68, 68, 68\);/);
    });

});
