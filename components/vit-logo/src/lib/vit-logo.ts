import { Component, Input } from '@angular/core';

@Component({
    selector: 'vit-logo',
    templateUrl: './vit-logo.html',
    styleUrls: ['./vit-logo.scss']
})
export class VitLogoComponent {

    @Input() image: string;             // Path to the image file.
    @Input() title?: string = 'Logo';   // Alt text and text on hover.
    @Input() compact?: boolean;         // Compact version of the logo.
    @Input() width?: number;            // Specific fixed width of the logo.
    @Input() height?: number;           // Specific fixed height of the logo.
    @Input() nightMode?: boolean;       // Night mode - adds specific class to switch colours or image.
    @Input() background?: string;       // Custom color of the background.

}
