import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { VitLogoComponent } from './vit-logo';

@NgModule({
    declarations: [ VitLogoComponent ],
    imports: [ CommonModule, MatButtonModule ],
    exports: [ VitLogoComponent ]
})
export class VitLogoModule { }
