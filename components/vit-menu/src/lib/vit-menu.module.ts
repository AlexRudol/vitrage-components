import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VitMenuComponent } from './vit-menu';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

@NgModule({
    declarations: [ VitMenuComponent ],
    imports: [ CommonModule, MatButtonModule, MatListModule, MatIconModule ],
    exports: [ VitMenuComponent ]
})
export class VitMenuModule { }
