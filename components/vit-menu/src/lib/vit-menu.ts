import { Component, EventEmitter, HostBinding, Input, Output, ViewEncapsulation } from '@angular/core';
import { MenuItemModel } from './menu-item.model';

@Component({
    selector: 'vit-menu',
    templateUrl: './vit-menu.html',
    styleUrls: ['./vit-menu.scss', './vit-menu.theme.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VitMenuComponent {

    // Modes
    @Input() compactMode?: boolean;         // Toggle the Panel compact mode
    @Input() nightMode?: boolean;           // Night mode of the Panel - switches colors

    // Content
    @Input() navigation?: MenuItemModel[];          // Navigation block (main menu)
    @Input() alwaysShowIcons?: boolean = false;     // Show icons in both compact and default modes
    @Input() expandMultiple?: boolean = false;      // Allow to expand multiple items
    @Input() rightAligned?: boolean = false;        // Align icons and menu items to the right

    // Outputs
    @Output() selectMenuItemEvent: EventEmitter<Array<string>> = new EventEmitter();

    // Apply host classes
    @HostBinding('class')
    get classes(): string {
        return (this.compactMode ? 'compact ' : '')
            + (this.nightMode ? ' night ' : '');
    }

    constructor() { }

    /*
     * Menu item click event
     */
    selectMenuItem(clickedItemIndex: string) {
        const clickedParentItemIndex: number = +clickedItemIndex.split('-')[0];
        const clickedChildItemIndex: number = +clickedItemIndex.split('-')[1];

        this.navigation.forEach( (item: MenuItemModel, itemIndex: number) => {
            // Select an item
            if (itemIndex + 1 === clickedParentItemIndex) {
                // Select parent item
                if (clickedChildItemIndex === 0) {
                    item.selected = true;
                    // If has children - deselect all
                    if (item.children) {
                        item.expanded = true;
                        item.children.forEach( (childItem: MenuItemModel) => {
                            childItem.selected = false;
                        });
                    }
                    this.selectMenuItemEvent.emit([ item.link, item.target ]);
                }
                // Select child item
                else {
                    item.selected = false;
                    item.children.forEach( (childItem: MenuItemModel, childItemIndex: number) => {
                        if (childItemIndex + 1 === clickedChildItemIndex) {
                            childItem.selected = true;
                            this.selectMenuItemEvent.emit([ childItem.link, childItem.target ]);
                        }
                        else {
                            childItem.selected = false;
                        }
                    });
                }
            }
            // Deselect the root item and it's children
            else {
                item.selected = false;
                if (item.children) {
                    item.expanded = false;
                    item.children.forEach( (childItem: MenuItemModel) => {
                        childItem.selected = false;
                    });
                }
            }
        });
    }
    expandMenuItem(clickedItemIndex: number, event: Event) {
        this.navigation.forEach( (item: MenuItemModel, itemIndex: number) => {
            // Expand selected item
            if (itemIndex + 1 === clickedItemIndex) {
                item.expanded = !item.expanded;
            }
            // Collapse others if the multiple expand is not allowed
            else {
                if (!this.expandMultiple) {
                    item.expanded = false;
                }
            }
        });
        event.stopPropagation(); // Do not trigger the selectMenuItem
    }

}
