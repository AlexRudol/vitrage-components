export interface MenuItemModel {
    separator?: boolean;    // Not an item, just a separator
    class?: string;         // Custom css classes
    label: string;          // Text of the item
    icon: string;           // Icon of the item for the compact mode
    link: string;           // Link of the menu item
    target?: string;        // Same or new page (tab)
    expanded?: boolean;     // Is item expanded showing all children?
    selected?: boolean;     // Is item currently selected?
    children?: MenuItemModel[]; // Another menu items inside
}
