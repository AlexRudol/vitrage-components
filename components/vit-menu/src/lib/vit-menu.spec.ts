import { createComponentFactory, Spectator, SpectatorFactory } from '@ngneat/spectator/jest';
import { fakeAsync } from '@angular/core/testing';
import { VitMenuComponent } from './vit-menu';
import { MenuItemModel } from './menu-item.model';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

const navigationSource: MenuItemModel[] = require('../../../../stories/components/shared/menu.json');

describe('VitMenuComponent', () => {
    let spectator: Spectator<VitMenuComponent>;
    const createComponent: SpectatorFactory<VitMenuComponent> = createComponentFactory({
        component: VitMenuComponent,
        imports: [ MatButtonModule, MatIconModule, MatListModule ]
    });

    beforeEach(() => spectator = createComponent());

    it('should create the Side Panel', () => {
        expect(spectator).toBeTruthy();
    });


    // Menu items and classes
    it('has Main Menu', () => {
        spectator.setInput('navigation', navigationSource);
        expect(spectator.query('.mat-list-item:nth-child(1) .label').innerHTML)
            .toBe('Item 1');
        expect(spectator.query('.mat-list-item:nth-child(2) .label').innerHTML)
            .toBe('Item 2');
        expect(spectator.query('.mat-list-item:nth-child(5)').classList[0])
            .toBe('sub-item');
        expect(spectator.query('.mat-list-item:nth-child(5) .label').innerHTML)
            .toBe('Item 3-2');
        expect(spectator.query('.separator:nth-child(7)').innerHTML)
            .toBe('&nbsp;');
        expect(spectator.query('.mat-list-item:nth-child(10) .label').innerHTML)
            .toBe('Item 4-2');
        expect(spectator.query('.mat-list-item:nth-child(11)'))
            .toBeFalsy();
    });
    it('has custom class for a menu item', () => {
        spectator.setInput('navigation', navigationSource);
        expect(spectator.query('.mat-list-item:nth-child(1)').classList.contains('custom-class'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(2)').classList.contains('custom-class'))
            .toBeTruthy();
    });

    // Click and expand
    it('selects Menu Item on click', () => {
        spectator.setInput('navigation', navigationSource);
        spectator.click('.mat-list-item:nth-child(2)');
        expect(spectator.query('.mat-list-item:nth-child(2)').classList.contains('selected'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(1)').classList.contains('selected'))
            .toBeFalsy();
    });
    it('selects Menu Sub-item on click', fakeAsync(() => {
        spectator.setInput('navigation', navigationSource);
        spectator.click('.mat-list-item:nth-child(1)');
        expect(spectator.query('.mat-list-item:nth-child(1)').classList.contains('selected'))
            .toBeTruthy();
        spectator.click('.mat-list-item:nth-child(4)');
        spectator.component.expandMenuItem(3, new Event('click'));
        spectator.tick(500);
        expect(spectator.query('.mat-list-item:nth-child(4)').classList.contains('selected'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(5)').classList.toString().includes('selected'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(3)').classList.contains('active'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(9)').classList.contains('active'))
            .toBeFalsy();
    }));
    it('toggle Menu Sub-item on click', () => {
        spectator.setInput('navigation', navigationSource);
        // The Item 1 is selected and the Item 3 is collapsed:
        spectator.click('.mat-list-item:nth-child(1)');
        expect(spectator.query('.mat-list-item:nth-child(1)').classList.contains('selected'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(3)').classList.contains('selected'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(3) .toggle-expand').classList.contains('open'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(5)').classList.contains('active'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(5)').classList.contains('hidden'))
            .toBeTruthy();
        // The Item 1 is still selected and the Item 3 now collapsed
        spectator.click('.mat-list-item:nth-child(3) .toggle-expand');
        expect(spectator.query('.mat-list-item:nth-child(3)').classList.contains('selected'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(3) .toggle-expand').classList.contains('open'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(5)').classList.contains('active'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(5)').classList.contains('hidden'))
            .toBeFalsy();
    });

    // Icons
    it('has icons in Compact Mode only', () => {
        spectator.setInput('navigation', navigationSource);
        spectator.setInput('compactMode', true);
        expect(spectator.query('.mat-list-item:nth-child(1) .label').classList.contains('hidden'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(1) .label-icon').classList.contains('hidden'))
            .toBeFalsy();
        spectator.setInput('compactMode', false);
        expect(spectator.query('.mat-list-item:nth-child(1) .label').classList.contains('hidden'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(1) .label-icon').classList.contains('hidden'))
            .toBeTruthy();
    });
    it('has icons always', () => {
        spectator.setInput('navigation', navigationSource);
        spectator.setInput('alwaysShowIcons', true);
        spectator.setInput('compactMode', true);
        expect(spectator.query('.mat-list-item:nth-child(1) .label').classList.contains('hidden'))
            .toBeTruthy();
        expect(spectator.query('.mat-list-item:nth-child(1) .label-icon').classList.contains('hidden'))
            .toBeFalsy();
        spectator.setInput('compactMode', false);
        expect(spectator.query('.mat-list-item:nth-child(1) .label').classList.contains('hidden'))
            .toBeFalsy();
        expect(spectator.query('.mat-list-item:nth-child(1) .label-icon').classList.contains('hidden'))
            .toBeFalsy();
    });

    // OUTPUT - Menu item click
    it('trigger event on Menu Item click', () => {
        let output: string;
        spectator.setInput('navigation', navigationSource);
        spectator.output('selectMenuItemEvent').subscribe((result: string) => (output = result));
        spectator.click('.mat-list-item:nth-child(1)');
        expect(output).toEqual(['#link1', undefined]); // Clicked link + link target (undefined)
    });

});
