import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { VitButtonComponent } from './vit-button';

describe('AppComponent', (): void => {

    beforeEach( async((): void => {
        TestBed.configureTestingModule({
            declarations: [ VitButtonComponent ],
            imports: [ MatButtonModule, MatIconModule ]
        }).compileComponents();
    }) );

    it('should create the app', async((): void => {
        const fixture: ComponentFixture<VitButtonComponent> = TestBed.createComponent(VitButtonComponent);
        const app: boolean = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }) );

} );
