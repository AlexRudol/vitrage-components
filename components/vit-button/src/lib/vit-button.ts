import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';

export enum ButtonType {
    default = 'default',    // stroke (contour)
    primary = 'primary',    // solid (flat button)
    lite    = 'lite'        // no borders, no fill
}

@Component({
    selector: 'vit-button',
    templateUrl: './vit-button.html',
    styleUrls: ['./vit-button.scss'],
    encapsulation: ViewEncapsulation.None
})
export class VitButtonComponent {

    // Types
    @Input() type?: ButtonType;     // The button look
    @Input() compact?: boolean;     // Small, more compact, version of the button
    @Input() wide?: boolean;        // Make the button wider

    // Inner elements
    @Input() label: string;         // Text inside
    @Input() icon?: string;         // Show an icon
    @Input() iconRight?: boolean;   // The icon is at the right side
    @Input() tooltip?: string;      // Hint text on hover

    // Status
    @Input() disabled?: boolean;    // Button is disabled
    @Input() loading?: boolean;     // Show the loading indicator inside the button

    constructor() {
        // Default values
        this.type = this.type ? this.type : ButtonType.default;
    }

}
