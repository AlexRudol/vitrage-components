import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { VitButtonComponent } from './vit-button';

@NgModule({
    declarations: [ VitButtonComponent ],
    imports: [ CommonModule, MatButtonModule, MatIconModule ],
    exports: [ VitButtonComponent ]
})
export class VitButtonModule { }
