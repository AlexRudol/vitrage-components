const { resolve } = require('path');

module.exports = {
    preset: 'jest-preset-angular',
    setupFilesAfterEnv: [
        '<rootDir>/setupJest.ts'
    ],
    roots: [
        '<rootDir>/components'
    ],
    // Mapping paths the same way as in the "compilerOptions > paths" section of the tsconfig.json
    moduleNameMapper: {
        '^@components/(.*)$': resolve(__dirname, './components/$1')
    }
};

