import { moduleMetadata } from '@storybook/angular';
import { Meta, Preview, Story } from '@storybook/addon-docs/blocks';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { VitMenuComponent } from '../../components/vit-menu/src/lib/vit-menu';
import { VitSidePanelComponent } from '../../components/vit-side-panel/src/lib/vit-side-panel';

import { version } from '../../components/vit-side-panel/package.json';
export const componentVersion = version;

export const logo = 'sample<br>logo';
export const logoCompact = 'SL';
export const sampleMessage = 'sample<br>side panel<br>message<br>instead of<br>navigation';
import navigationSource from './shared/menu.json';
export const navigation = JSON.stringify(navigationSource);

<Meta title="Components | Side Panel"
    decorators={[ moduleMetadata({
        declarations: [ VitMenuComponent, VitSidePanelComponent ],
        imports: [ MatButtonModule, MatIconModule, MatListModule ]
    })]}
/>

# Side Panel

<p><code>v {componentVersion}</code></p>

A side panel with the logo, message, navigation and other elements inside.
Can be expanded or collapsed (compact mode).

Panel content-related versions:
* Just a Logo (maintenance or dummy page)
* Logo + Message (simple static content)
* Logo + Main Menu


### Inputs

| Name | Type | Values | Description |
| ---- | ---- | ------ | ----------- |
| ** MODES ** |
| **compactMode**           | `boolean`     |           | Toggle the Panel compact mode. |
| **nightMode**             | `boolean`     |           | Night mode of the Panel - switches colors. |
| ** CONTENT ** |
| **toggle**                | `boolean`     | *true*    | Show or hide Panel toggle. |
| **logo**                  | `string`      |           | Logo (html or text). |
| **logoCompact**           | `string`      |           | Compact logo (html or text) to show in compact Panel mode. |
| **message**               | `string`      |           | Show a static message in the Panel. |
| **navigation**            | `MenuItemModel[]` |       | Navigation block (main menu). |
| **alwaysShowIcons**       | `boolean`     |           | Show icons in both compact and default modes. |
| **expandMultiple**        | `boolean`     | _false_   | Allow to expand multiple items. |
| **rightAligned**          | `boolean` | _false_ | Align content inside to the right, send the same parameter to Menu.|
| **backgroundHTML**        | `string`      |    | Background - html or image to add static or interactive background. |

### Outputs

| Name | Type | Values | Description |
| ---- | ---- | ------ | ----------- |
| **selectMenuItemEvent** | `EventEmitter<Array<string>>` | *null* | Menu item is selected. |
| **logoClickEvent** | `EventEmitter<LogoMode>` | *null* | User clicked the Logo. |
| **toggleClickEvent** | `EventEmitter<boolean>` | *null* | The panel is toggled. |


#### Mode and the screen size:
| mode                       | Mobile      | Tab portrait | Tab landscape | Desktop | Ultra-wide |
| -------------------------- | ----------- | ------------ | ------------- | ------- | ---------- |
| **default - full screen**  | —           | —            | —             | —       | > default  |
| **default**                | > collapsed | > collapsed  | —             | —       | —          |
| **compact**                | > collapsed | stay or > collapsed | —      | —       | > default  |
| **compact - mobile**       | —           | stay or > compact | > compact | > compact | > compact |


### Examples

#### Default
Default state of the panel. Auto size - compact mode if the view area is narrow. With navigation and no toggle.
<Preview>
    <Story name="Default" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}"
                [toggle]="false" [navigation]='${navigation}' ></vit-side-panel>`
        }}
    </Story>
</Preview>

#### Compact with toggle
Compact mode of the panel by default. With navigation and the toggle.
<Preview>
    <Story name="Compact" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}" [compactMode]="true"
                [navigation]='${navigation}' ></vit-side-panel>`
        }}
    </Story>
</Preview>

#### Message
The panel with the message inside. No navigation, no toggle.
<Preview>
    <Story name="Message" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}" [compactMode]="false"
                [toggle]="false" message='${sampleMessage}' ></vit-side-panel>`
        }}
    </Story>
</Preview>

#### Message right
The panel with the message inside. Right aligned. No navigation, no toggle.
<Preview>
    <Story name="Message right" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}" [compactMode]="false"
                [rightAligned]="true" [toggle]="false" message='${sampleMessage}' ></vit-side-panel>`
        }}
    </Story>
</Preview>

#### Right with menu
The default right aligned panel. With navigation and toggle. Icons are always visible.
<Preview>
    <Story name="Right with menu" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}"
                [rightAligned]="true" [navigation]='${navigation}' [alwaysShowIcons]="true" ></vit-side-panel>`
        }}
    </Story>
</Preview>

#### Night
The panel in the night mode. No navigation, no toggle.
<Preview>
    <Story name="Night" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}" [compactMode]="false"
                [nightMode]="true" [toggle]="false" message='${sampleMessage}' ></vit-side-panel>`
        }}
    </Story>
</Preview>

#### Night with menu
The panel in the night mode. With navigation and toggle. Icons are always visible.
<Preview>
    <Story name="Night with menu" height="500px" >
        {{
            template: `<vit-side-panel logo="${logo}" logoCompact="${logoCompact}"
                [nightMode]="true" [navigation]='${navigation}' [alwaysShowIcons]="true" ></vit-side-panel>`
        }}
    </Story>
</Preview>


### Theming

Import the `vit-side-panel.theme.scss` file into a component and override variables:

```scss
// Default colors
$colorPanelBackground:      hsl(240, 13%, 91%);
$colorText:                 hsl(0, 0%, 40%);

// Night colors
$colorPanelBackgroundNight: hsl(225, 3%, 29%);
$colorTextNight:            hsl(0, 0%, 100%);
```
